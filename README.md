# AngularCertification

### A STG Angular Certification Module

In this project you will be creating a simple Angular App that can search for and display details of movies and actors. Since this project is designed to test your Javascript/Typescript fundamentals and Angular skills, there will be comments and instructions provided to help with non-Angular specific tools.

## IMPORTANT --

To more clearly showcase your Angular skill, you will need to follow a couple of procedural steps.

1. Before you work on each section of this certification you will need create a new branch specifically for that section.
1. You will then need to commit and push your work to your repository on Bitbucket at least daily while working on this certification. While there is no official time limit on completing this certification we want to see the progress that you have made. This does not mean that you need to make a commit daily, it simply means that any day that you do work on the certification you should make a commit and push that work by the end of the day.
1. After each component section is completed you will need to save and push your code **AND** make a pull request to merge your code to your repository's master branch for each section of your work. ***(Please do not delete the merged branch from your repository!)***
1. Be sure to add comments in your code that demonstrate your knowledge of Javascript and Angular fundamentals as listed at the end of this file. This is necessary to show your knowledge of these important concepts and to assist the certification reviewer in verifying that you have completed all of the tasks in this Angular certification. You'll probably find that it is easier and more convenient to make these comments as you work on the certification rather than wait until the work has been completed.

We have provided a few resources to assist you with understanding the fundamentals of Javascript and Angular. You can find these resourcs in our [Angular Certification Resources](./Certification.md).

You will want to follow the [Angular style guide](https://angular.io/guide/styleguide) for this project. Using the Angular CLI to generate your modules, services, components, etc. will help you follow the style guide.

Do not spend a lot of time on styling this project. We want to see enough styling to know that you understand how to style Angular components but we do not want you to spend a lot of time on styling as styling is not the focus of this certification.

## Tour of Heroes

If this is your first time working with Angular it is highly recommend completing the [Tour of Heroes tutorial](https://angular.io/tutorial) first. This provides a great introduction to each of the parts of Angular. It will serve as a foundation for the following sections when we dive into each area in more detail. The completed tutorial does not need to be included in your final project. If you already have some experience with Angular you can skip this part.

## Initializing App

1. Fork the [AngularCertification](https://bitbucket.org/stgconsulting/angularcertification/) repository. ([How to fork with Bitbucket](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html))
1. Clone that repository onto your computer.
1. Create a new branch called `initializing` and checkout this branch. This is where you will begin working on this project.
1. Spin up a new Angular app using the Angular CLI.  Use the name `movie-app`.
1. Set up your new app by installing the following npm packages:
    - @angular/material
    - @angular/cdk
    - @angular/animations

More information is available for use of `Angular Material` at [https://material.angular.io](https://material.angular.io).

1. This project will be using [The Movie Database API v3](https://developers.themoviedb.org/3/getting-started/introduction). To work with this you will need to obtain an API key by creating an account with them. Follow the instructions [here](https://developers.themoviedb.org/3/getting-started/introduction) to obtain your API key.
1. Save your work and push these changes to Bitbucket.
1. Create a Pull Request (PR) for your code and merge it with the `master` branch. You do not need to approve your pull request, these are used by the certification reviewer to see how you are progressing, and provides them with the ability to comment directly on your code. ([How to create a pull request with Bitbucket](https://confluence.atlassian.com/bitbucket/create-a-pull-request-945541466.html))

## Authentication

1. Create a new branch called `routing` and check out this branch.
1. Create two new modules called `auth` and `dashboard`.
1. Inside the dashboard module create a dashboard component.
1. Inside the auth module create an auth service and a login component.
   - The login component should have an username and password field using `ReactiveForms`.
   - The password input should have a way for the user to toggle masking and showing the password.
   - The login button should be disabled until a username and password have been entered.
   - Clicking the login button will call a method on your auth service to login the user.
   - Once logged in the user should be redirected to `/dashboard` which will be created below.
   - To simulate an HTTP call, in place of making a request to some authentication endpoint use this
```typescript
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

...
return of({/**some object representing your user that just logged in**/}).pipe(delay(1500));
...
```
5. Create a `/login` route on the auth module that displays the login component.
   - The `/login` route should redirect to `/dashboard` if the user is logged in.
1. Create a `/dashboard` route on the dashboard module that displays the dashboard component.
   - The `/dashboard` route should not be accessible if the user is not logged in and should redirect to `/login`.
   - These types of checks and redirects should be done using guards.
1. In your app component, create a header or some buttons for logging in/out and navigating around.
1. Save your work and push these changes to Bitbucket.
1. Create a Pull Request (PR) for your code and merge it with the `master` branch.

## Movie Search

1. Create a new branch called `movie` and check out this branch.
1. Create a new module called `search`.
1. Inside the search module create
   - A search service.
   - A movie search component.
   - A movie search filters component.
   - A movie search results component.
1. Create a `/search/movie` route in the search module that routes to the movie search component.
1. The movie search component will contain the filters and results components.
1. The filters component should use a reactive form to allow the user to enter:
   - A required search query.
   - An optional year.
   - The search button should be disabled until the required field is entered.
1. The results component should have the following:
   - A paginated table showing the results of the search.
   - Display the total number of results.
   - Should not allow moving to the next page if you are on the last page.
   - Recommend using the [Material Paginator](https://material.angular.io/components/paginator/overview).
   - If no results are found display a message to let the user know.
1. The form data should be reflected in the application URL, for example if a user searched for `back` with a year of `1985`, the URL should be updated to `/search/movie?query=back&year=1985`.
   - Your component can subscribe directly to the router, but to make the functionality sharable you may want to create an active filters service that subscribes to the router and emits the query strings when set.
1. The search service should perform the following actions:
   - Make an HTTP request to [The Movie Database API search endpoint](https://developers.themoviedb.org/3/search/search-movies).
   - It should not include the API key directly in the call. Since multiple services will require the API key you should create an HTTP interceptor in the app folder that is responsible for adding the API key to your requests.
   - Both the interceptor and the service should have at least one unit test each testing their functionality.
1. Add a link or button to your app navigation.
1. Save your work and push these changes to Bitbucket.
1. Create a Pull Request (PR) for your code and merge it with the `master` branch.

## Movie Details

1. Create a new branch called `details` and check out this branch.
1. Create a new module called `details`.
1. Inside the details module create a movie details component and service.
1. Inside the `app/` folder create a movie collection service.
1. Create a `/details/movie/:movieId` route that navigates to the movie details component.
1. The movie details component should display the following:
   - The title of the movie.
   - A truncated overview that displays the full thing on mouse over.
   - The movie poster ([The Movie Database API images](https://developers.themoviedb.org/3/getting-started/images)).
   - The background image using the `backdrop_path` property.
   - The list of production companies and their logos.
   - The release date, using a pipe to convert the format to `MM/DD/YYYY`.
   - If the user is logged in, a button that allows adding the movie to their collection.
1. The movie details service should perform the following actions:
   - Make an HTTP request to [The Movie Database API movie details endpoint](https://developers.themoviedb.org/3/movies/get-movie-details).
1. The movie collection service should perform the following actions:
   - Add movies to a collection.
   - Remove a movie from the collection.
   - Get the list of movies in the collection.
   - The list can be stored in memory or use a storage solution like localstorage or a cookie.
   - Add unit tests for this functionality.
1. Update the dashboard component to allow it to:
   - Display the list of movies in the user's collection.
   - Link to any of the movie details.
   - Remove a movie from the list.
   - Add unit tests for this component, using a mock in place of the movie collection service.
1. Update the movie search results table to link to the movie details page for the selected movie.
1. Save your work and push these changes to Bitbucket.
1. Create a Pull Request (PR) for your code and merge it with the `master` branch.

## Person Search

1. Create a `person-search` branch and check out the branch.
1. This time you will be responsible for structuring all of your code. Following the [Angular style guide](https://angular.io/guide/styleguide) add the ability to search for people.
1. The person search functionality should be similar to the movie search functionality.
    - You should be able to route to this search page.
    - The search query should be required. There are no optional filters for this search.
    - The search button should be disabled until the required field is entered.
    - The form data should be reflected in the application URL.
1. Save your work and push these changes to Bitbucket.
1. Create a Pull Request (PR) for your code and merge it with the `master` branch.

## Person Details

1. Create a `person-details` branch and check out the branch.
1. The person details functionality should be similar to the movie details functionality.
1. The person details page should display the following information:
    - The person's name.
    - The list of their `also_known_as` names.
    - The birthday, formatted as `MM/DD/YYYY`.
    - A truncated biography that displays the full thing on mouse over.
    - Their profile image.
    - A link to their homepage if available.
    - A list of their 5 most recent movie cast and crew credits.
    - For cast it should display the following:
        - The character name.
        - The title.
        - The movie poster.
    - For crew it should display the following:
        - The department.
        - The title.
        - The movie poster.
    - If either of these credits are empty, display `No (cast | crew) credits found`
    - If the user is logged in, a button that allows adding the person to their list of favorites.
        - This is a separate collection from the movie list, but should also be displayed from the user's profile.
1. Save your work and push these changes to Bitbucket.
1. Create a Pull Request (PR) for your code and merge it with the `master` branch.

## Final Steps

1. Create a `final` branch and check out the branch.
1. Create a single component that:
    - Displays a button with the text `Add` if a movie or person is not currently in either user's collection.
    - Displays a button with the text `Remove` if a movie or person has already been added to a user's collection.
1. Clicking the add button will add the movie or person to their respective collection.
1. Clicking the remove button will remove the movie or person to their respective collection.
1. If a user is not logged in the button should not display.
1. Add the component to each row of the results table for both the movie and person results, so you can quickly add/remove items from your saved lists.
1. Use `Input` and `Output` to make this single component capable of working for both movie and persons, and other items in the future.
1. Make the `dashboard` module lazy loaded. All users of this app won't access this section, so we can lazy load it to decrease the main bundle size and improve the initial page loading.
1. Add a way to sort the list of movies and favorite people by alphabetical order in the dashboard.
1. Ensure you have no linting errors.
1. Ensure all of your unit tests pass.
1. Save your work and push these changes to Bitbucket.
1. Create a Pull Request (PR) for your code and merge it with the `master` branch.

## Finish Project
1. Create a `comments` branch.
2. Inside of this branch you will (if you haven't already) add comments to your code to demostrate your use and knowledge of Javascript fundamentals as well as Angular basics. This should not be hard, but it will help us recognize your knowledge and use of ES6 and ES7 basics. You do not need to identify every instance, just 1-2 instances to show you know and understand these concepts.
3. In your code you should add comments to demonstrate these **Javascript** and **Typescript** fundamentals:
    - Closures
    - `this` keyword
    - Arrow functions
    - Array Functions
        - Map
        - Filter
        - Reduce
    - Destructuring
    - Spread Operator
    - Promises
    - Blocked-scoped variables `let` and `const`
    - Interfaces
    - Type definitions
4. Comments should be proceeded with a `#` and should look something like this:

```
  // #closure
  function myClosure() = {...}
```

or this:

```
  /* #map function */
  myArray.map(...);
```
5. In your code you should add comments to demonstrate these **Angular** basics:
    - Modules
    - Components
    - Inputs
    - Outputs
    - Lifecycle hooks
    - Forms
    - Validation (built-in validators, custom validators)
    - Directives
    - Pipes (built in, custom)
    - Services
    - Observables
    - Guards
6. Comments should be styled similar to the Javascript/Typescript comments.
7. Remove all console.log statements from your code.
8. Save your work and push these changes to Bitbucket.
9. Create a Pull Request (PR) for your code and merge it with the `master` branch.
10. Email the certification reviewer that your have completed the certification project and that it is now ready for review!

**Congratulations**
You have completed the Angular Certification!
