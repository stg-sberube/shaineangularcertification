// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import * as CryptoJS from 'crypto-js';

export const environment = {
  production: false,
  movie_api_key: 'a3c60ce7e6b33b799b979c313d91dbce',
  movie_base_url: 'https://api.themoviedb.org/3/',
  movie_api_base_query_params: [{ key: 'language', value: 'en-US' }, { key: 'include_adult', value: 'false' }],
  encryption_salt: 'AcEg1@3$5',
  encryption_keySize: 64,
  encryption_mode: CryptoJS.mode.CBC,
  encryption_padding: CryptoJS.pad.Pkcs7,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
