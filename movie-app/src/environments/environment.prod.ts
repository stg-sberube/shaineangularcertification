import * as CryptoJS from 'crypto-js';

export const environment = {
  production: true,
  movie_api_key: 'a3c60ce7e6b33b799b979c313d91dbce',
  movie_base_url: 'https://api.themoviedb.org/3/',
  movie_api_base_query_params: [{ key: 'language', value: 'en-US' }, { key: 'include_adult', value: 'false' }],
  encryption_salt: 'r@nd0mSa1t!',
  encryption_keySize: 64,
  encryption_mode: CryptoJS.mode.CBC,
  encryption_padding: CryptoJS.pad.Pkcs7,
};
