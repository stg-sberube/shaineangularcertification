import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { PageSpinnerService } from '@shared/services/page-spinner.service';
import { Subscription } from 'rxjs';

import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  formGroup: FormGroup;
  hide: boolean = true;
  subs: Subscription[] = [];
  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<LoginComponent>,
    private auth: AuthService,
    private router: Router,
    private ps: PageSpinnerService
  ) {}

  ngOnInit() {
    this.formGroup = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
    });
  }

  close(): void {
    this.dialogRef.close();
  }

  submit(): void {
    this.ps.display = true;
    const submission = this.formGroup.value;
    const loginCall = this.auth.login(submission.username, submission.password).subscribe(login => {
      setTimeout(() => {
        if (login) {
          this.close();
          this.ps.display = false;
          this.router.navigate([this.router.url]);
        } else {
          this.formGroup.reset();
          this.formGroup.setErrors({ submitFail: true });
          this.ps.display = false;
        }
      }, 1500);
    });
    this.subs.push(loginCall);
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
