import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '@shared/interfaces/user';
import { PageSpinnerService } from '@shared/services/page-spinner.service';
import { Subscription } from 'rxjs';
import { delay, tap } from 'rxjs/operators';

import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  formGroup: FormGroup;
  hide: boolean = true;
  subs: Subscription[] = [];
  usernameValid: boolean = null;
  emailValid: boolean = null;
  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private ps: PageSpinnerService
  ) {}

  ngOnInit() {
    this.formGroup = this.fb.group({
      firstname: [null, Validators.required],
      lastname: [null, Validators.required],
      username: [null, Validators.required],
      password: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
    });

    const username = this.formGroup
      .get('username')
      .valueChanges.pipe(
        delay(500),
        tap(_ => (this.ps.display = true))
      )
      .subscribe(value => {
        setTimeout(_ => {
          // #validator This is custom validation ensuring we don't have two same usernames
          if (this.auth.isUniqueUser('username', value)) {
            this.usernameValid = true;
          } else {
            this.usernameValid = false;
          }
          this.ps.display = false;
        }, 1500);
      });
    const email = this.formGroup
      .get('email')
      .valueChanges.pipe(
        delay(1000),
        tap(_ => (this.ps.display = true))
      )
      .subscribe(value => {
        setTimeout(_ => {
          if (this.auth.isUniqueUser('email', value)) {
            this.emailValid = true;
          } else {
            this.emailValid = false;
          }
          this.ps.display = false;
        }, 1500);
      });
    this.subs.push(username, email);
  }

  cancel(): void {
    this.router.navigate(['']);
  }

  submit(): void {
    this.ps.display = true;
    const submission = this.formGroup.value;
    // #spread-operator Here we're simply grabbing all form values and casting them to a User object to submit
    const auth = this.auth.register({ ...submission } as User).subscribe(register => {
      setTimeout(() => {
        if (register) {
          this.ps.display = false;
          this.router.navigate(['dashboard']);
        } else {
          this.formGroup.reset();
          this.formGroup.setErrors({ submitFail: true });
          this.ps.display = false;
        }
      }, 1500);
    });
    this.subs.push(auth);
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
