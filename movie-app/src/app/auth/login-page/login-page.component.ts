import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@shared/services/auth.service';
import { PageSpinnerService } from '@shared/services/page-spinner.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit, OnDestroy {
  formGroup: FormGroup;
  hide: boolean = true;
  subs: Subscription[] = [];
  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private ps: PageSpinnerService
  ) {}

  ngOnInit() {
    this.formGroup = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
    });
  }

  submit(): void {
    this.ps.display = true;
    const submission = this.formGroup.value;
    const loginCall = this.auth.login(submission.username, submission.password).subscribe(login => {
      setTimeout(() => {
        if (login) {
          this.ps.display = false;
          this.router.navigate(['dashboard']);
        } else {
          this.formGroup.reset();
          this.formGroup.setErrors({ submitFail: true });
          this.ps.display = false;
        }
      }, 1500);
    });
    this.subs.push(loginCall);
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
