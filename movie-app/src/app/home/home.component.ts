import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@shared/services/auth.service';
import { Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';

import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnDestroy {
  authed: boolean;
  subs: Subscription[] = [];
  configBaseURL$: Observable<string> = this.homeService.configuration$.pipe(
    distinctUntilChanged(),
    map(config => config.images.base_url),
    filter(data => data !== null)
  );
  constructor(private auth: AuthService, private router: Router, public homeService: HomeService) {
    const a = this.auth.isAuthenticated$.subscribe(authed => {
      this.authed = authed;
    });
    this.subs.push(a);
  }

  nextSlide(event) {
    if (event) {
      this.homeService.next();
    }
  }

  previousSlide(event) {
    if (event) {
      this.homeService.previous();
    }
  }

  goToSlide(event) {
    this.homeService.goToSlide(event);
  }

  navigateToLogin() {
    this.router.navigate(['auth', 'login']);
  }

  navigateToRegister() {
    this.router.navigate(['auth', 'register']);
  }

  navigateToMorTSearch() {
    this.router.navigate(['search', 'movie']);
  }

  navigateToPersonSearch() {
    this.router.navigate(['search', 'person']);
  }

  goToMovieDetails(id: number) {
    this.router.navigate(['details', 'movie', id]);
  }

  logout(): void {
    this.auth.logout().subscribe(logout => {
      if (!logout) {
        alert('Error logging out!');
      }
      this.router.navigate(['']);
    });
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => subscription.unsubscribe());
  }
}
