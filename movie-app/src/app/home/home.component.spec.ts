import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule, MatCardModule, MatIconModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { CarouselNavigationComponent } from '@shared/components/carousel-navigation/carousel-navigation.component';
import { CarouselOverlayComponent } from '@shared/components/carousel-overlay/carousel-overlay.component';
import { CarouselSlideComponent } from '@shared/components/carousel-slide/carousel-slide.component';
import { CarouselTitleComponent } from '@shared/components/carousel-title/carousel-title.component';
import { CarouselComponent } from '@shared/components/carousel/carousel.component';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        CarouselComponent,
        CarouselSlideComponent,
        CarouselNavigationComponent,
        CarouselTitleComponent,
        CarouselOverlayComponent,
      ],
      imports: [HttpClientTestingModule, RouterTestingModule, MatButtonModule, MatIconModule, MatCardModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
