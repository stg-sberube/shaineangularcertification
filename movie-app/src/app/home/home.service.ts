import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router, UrlSerializer } from '@angular/router';
import { CarouselSlide } from '@shared/interfaces/carousel-slide';
import { HomeResponseMovies, HomeResponseTV } from '@shared/interfaces/home-response';
import { MoviesDetailsConfiguration } from '@shared/interfaces/movies-details-configuration';
import { EnvironmentService } from '@shared/services/environment.service';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  // Environment has urls and stuff necessary to use when hitting the API
  environment = this.env.getEnvironment();
  // By using BehaviorSubjects here, we can control what page we pull from the API for each of the lists.
  latestTvPage: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  latestTvPage$: Observable<number> = this.latestTvPage.asObservable();
  nowPlayingPage: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  nowPlayingPage$: Observable<number> = this.nowPlayingPage.asObservable();
  upcomingPage: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  upcomingPage$: Observable<number> = this.upcomingPage.asObservable();
  // Get TV airing today
  latestTv$: Observable<CarouselSlide[]> = this.latestTvPage$.pipe(
    switchMap(page => {
      const url = this.createUrl(this.environment.movie_base_url, { page }, ['tv', 'airing_today']);
      /* #map The first map takes the results from the call and then
      turns them into something Typescript will understand */
      /* #map The next map takes the results from above and outputs
      them to a new object stream, this new object is what the carousel
      understands */
      return this.http.get(url).pipe(
        map((response: HomeResponseTV) => response.results),
        map(results =>
          results.map(
            (result, index) =>
              ({
                id: result.id,
                title: result.name,
                content_image: result.backdrop_path,
                order: index + 1,
                overlay: {
                  content: result.overview,
                  release_date: result.first_air_date,
                },
              } as CarouselSlide)
          )
        )
      );
    })
  );
  // Get Now playing movies (Movies in theaters)
  nowPlayingMovies$: Observable<CarouselSlide[]> = this.nowPlayingPage$.pipe(
    switchMap(page => {
      /* #switchMap This takes one stream and then outputs a completely
      different stream, in this case, we take the page number and feed it
      to the http call, we then map the results of that call and then turn
      it into an object we can consume. */
      const url = this.createUrl(this.environment.movie_base_url, { page }, ['movie', 'now_playing']);
      return this.http.get(url).pipe(
        map((response: HomeResponseMovies) => response.results),
        map(results =>
          results.map(
            (result, index) =>
              ({
                id: result.id,
                title: result.title,
                content_image: result.backdrop_path,
                order: index + 1,
                overlay: {
                  content: result.overview,
                  release_date: result.release_date,
                },
              } as CarouselSlide)
          )
        )
      );
    })
  );
  // Get Upcoming movies
  /* #closure These are all styled as closures, when using them they are run,
  otherwise a pointer is stored and JS will know when and where to run them */
  upcomingMovies$: Observable<CarouselSlide[]> = this.upcomingPage$.pipe(
    switchMap(page => {
      const url = this.createUrl(this.environment.movie_base_url, { page }, ['movie', 'upcoming']);
      return this.http.get(url).pipe(
        map((response: HomeResponseMovies) => response.results),
        map(results =>
          results.map(
            (result, index) =>
              ({
                id: result.id,
                title: result.title,
                content_image: result.backdrop_path,
                order: index + 1,
                overlay: {
                  content: result.overview,
                  release_date: result.release_date,
                },
              } as CarouselSlide)
          )
        ),
        map(movies =>
          movies
            .filter(movie => {
              /* #filter I put a filter on here because for 'upcoming' movies, I only want to see
            ones that are on the schedule, not ones that are already released. */
              const current = new Date(movie.overlay.release_date);
              const today = new Date();
              return current.getTime() > today.getTime();
            })
            .sort((a, b) => {
              // #sort This sort gives us the closest movies to release.
              const aDate = new Date(a.overlay.release_date);
              const bDate = new Date(b.overlay.release_date);
              return aDate.getTime() > bDate.getTime() ? 1 : -1;
            })
        )
      );
    })
  );
  slides$: Observable<CarouselSlide[]> = combineLatest([
    this.nowPlayingMovies$.pipe(filter(movies => movies !== null)),
    this.latestTv$.pipe(filter(shows => shows !== null)),
  ]).pipe(
    tap(([movies, tv]) => {
      /* #destructuring This is a good example of destructuring, I basically passed everything in as a single variable,
      but then I took it back out and named the variables so I could work with them */
      if (this.movieIndices.length === 0) {
        const noUpcoming = movies.filter(movie => {
          // Filter to get rid of 'upcoming' movies
          const current = new Date(movie.overlay.release_date);
          const today = new Date();
          return current.getTime() < today.getTime();
        });
        // We don't want to include movies that can show up in upcoming films here, only those that have already been released.
        this.movieIndices = this.getRandomIndices(noUpcoming, noUpcoming.length - 1, 4);
      }
      // We don't have to do the filtering on TV shows though, we don't have anything 'upcoming' there.
      if (this.tvIndices.length === 0) {
        this.tvIndices = this.getRandomIndices(tv, tv.length - 1, 4);
      }
    }),
    map(([movies, tv]) => {
      // #destructuring Here we're working with variables again, but by the end of this call we have a singular array
      const slides: CarouselSlide[] = [];
      movies.forEach(movie => {
        if (this.movieIndices.includes(movie.id)) {
          movie.title = 'Recent Release: '.concat(movie.title);
          slides.push(movie);
        }
      });
      tv.forEach(show => {
        if (this.tvIndices.includes(show.id)) {
          show.title = 'Currently Airing: '.concat(show.title);
          slides.push(show);
        }
      });
      return slides.sort((a, b) => {
        const aDate = new Date(a.overlay.release_date);
        const bDate = new Date(b.overlay.release_date);
        return aDate.getTime() < bDate.getTime() ? 1 : -1;
      });
    }),
    map(slides => {
      let count = 1;
      const newSlides: CarouselSlide[] = [];
      slides.forEach(slide => {
        slide.order = count;
        newSlides.push(slide);
        count = count + 1;
      });
      return newSlides;
    }),
    tap(slides => {
      // #tap This little beauty allows us to halt the current stream for a bit of side-processing for a moment
      // using this to set the currentslide if it is not set
      if (this.currentSlide.getValue() === 0 || this.currentSlide.getValue() === null) {
        const first = slides.find(slide => slide.order === 1);
        this.currentSlide.next(first.id);
      }
    })
  );
  private currentSlide: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  currentSlide$: Observable<number> = this.currentSlide.asObservable();
  // MaxOrder ID should be the last element of the slides array, given that it is sorted
  maxOrderId$: Observable<number> = this.slides$.pipe(
    filter(slides => slides !== null),
    map(slides => slides[slides.length - 1].id)
  );
  // #observable Using observables here we can automatically capture changes and propogate them to the carousel
  previousSlide$: Observable<number> = this.slides$.pipe(
    distinctUntilChanged(),
    withLatestFrom(this.currentSlide$, this.maxOrderId$),
    map(([slides, currentSlideId, maxOrderId]) => {
      const curSlide = slides.filter(slide => slide.id === currentSlideId)[0];
      let prevSlide: CarouselSlide;
      if (curSlide.order === 1) {
        prevSlide = slides.filter(slide => slide.id === maxOrderId)[0];
      } else {
        prevSlide = slides.filter(slide => slide.order + 1 === curSlide.order)[0];
      }
      return prevSlide.id;
    })
  );
  /* #observable This observable is getting the next slide to progress to given the position of the current slide. */
  // #distinctUntilChanged This prevents the observable from endlessly reloading
  /* #withLatestFrom Used this here specifically because I don't need to and don't care to watch changes to the
  current slide or maxOrderId, the only changes I'm concerned with would be on slides$ */
  /* #destructuring Another moment, this time we've combined three different observables into one, then destructured
  to work with the variables. */
  nextSlide$: Observable<number> = this.slides$.pipe(
    distinctUntilChanged(),
    withLatestFrom(this.currentSlide$, this.maxOrderId$),
    map(([slides, currentSlideId, maxOrderId]) => {
      const curSlide = slides.filter(slide => slide.id === currentSlideId)[0];
      let nextSlide: CarouselSlide;
      if (curSlide.id === maxOrderId) {
        nextSlide = slides.filter(slide => slide.order === 1)[0];
      } else {
        nextSlide = slides.filter(slide => slide.order - 1 === curSlide.order)[0];
      }
      return nextSlide.id;
    })
  );
  slideCount$: Observable<number> = this.slides$.pipe(map(slides => slides.length));
  // #http This call gets necessary data in order to display images throughout the app.
  configuration$: Observable<MoviesDetailsConfiguration> = this.http
    .get(this.environment.movie_base_url + 'configuration')
    .pipe(map((response: MoviesDetailsConfiguration) => response));
  movieIndices: number[] = [];
  tvIndices: number[] = [];
  constructor(
    private http: HttpClient,
    private env: EnvironmentService,
    private router: Router,
    private serializer: UrlSerializer
  ) {}

  // #function This nifty thing provides http query string parameters embedded into the url
  createUrl(baseUrl: string, params, additionalRoute?: string[]): string {
    this.environment.movie_api_base_query_params.forEach(param => {
      if (param.key === 'language') {
        params[param.key] = param.value;
      }
    });
    const tree = this.router.createUrlTree(additionalRoute ? additionalRoute : [], { queryParams: params });
    let paramString = this.serializer.serialize(tree);
    // check if first two letters in serializer.serialize() is /?, if yes, remove first character so that /? turns into ?
    paramString = paramString.substr(1);
    return baseUrl + paramString;
  }

  setLatestTvPage(page: number) {
    this.latestTvPage.next(page);
  }

  setNowPlayingPage(page: number) {
    this.nowPlayingPage.next(page);
  }

  setUpcomingPage(page: number) {
    this.upcomingPage.next(page);
  }

  setCurrentSlide(id: number) {
    this.currentSlide.next(id);
  }

  previous() {
    this.previousSlide$.subscribe(id => {
      this.setCurrentSlide(id);
    });
  }

  next() {
    this.nextSlide$.subscribe(id => {
      this.setCurrentSlide(id);
    });
  }

  goToSlide(id: number) {
    this.setCurrentSlide(id);
  }

  // This grabs random IDs from the list and returns them, this way the combination of films changes each time you visit the page.
  getRandomIndices(array: CarouselSlide[], maxIndex: number, size: number): number[] {
    // #block-scoped Using let whenever the variable will have it's value reassigned, tslint likes const everywhere else.
    let count = size;
    const idArray: number[] = [];
    let indicesArray: number[] = [];
    while (count > 0) {
      const index = Math.floor(Math.random() * (maxIndex + 1));
      if (!indicesArray.includes(index)) {
        indicesArray.push(index);
        indicesArray = indicesArray.sort((a, b) => (a > b ? 1 : -1));
        count = count - 1;
      }
    }
    indicesArray.forEach(index => {
      idArray.push(array[index].id);
    });
    return idArray;
  }

  getImageUrl(imagePath: string, baseUrl: string): string {
    if (imagePath && baseUrl) {
      return [baseUrl.replace(/^\/|\/$/g, ''), 'original', imagePath.replace(/^\/|\/$/g, '')].join('/');
    }
    return '';
  }
}
