import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchType } from '@shared/interfaces/search-type';
import { SessionRecentSearch } from '@shared/interfaces/session-recent-search';
import { AuthService } from '@shared/services/auth.service';
import { SearchService } from '@shared/services/search.service';
import { Observable, Subscription } from 'rxjs';

import { RecentSearchHistoryService } from '../recent-search-history.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnDestroy {
  type: SearchType;
  movieRecent$: Observable<SessionRecentSearch[]> = this.recentSearch.movieSearchHistory$;
  tvRecent$: Observable<SessionRecentSearch[]> = this.recentSearch.tvSearchHistory$;
  personRecent$: Observable<SessionRecentSearch[]> = this.recentSearch.personSearchHistory$;
  authed: boolean;
  subs: Subscription[] = [];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private recentSearch: RecentSearchHistoryService,
    private search: SearchService,
    public auth: AuthService
  ) {
    const r = this.route.params.subscribe(params => {
      if (params.type === 'movie') {
        this.type = SearchType.Movie;
      } else {
        this.type = SearchType.Person;
      }
    });
    this.subs.push(r);
    const temp = this.auth.isAuthenticated$.subscribe(authed => {
      this.authed = authed;
    });
    this.subs.push(temp);
  }

  addRecentSearchQueryParams(recentSearch: SessionRecentSearch) {
    const qParams = this.search.getQueryParams(recentSearch.params);
    this.recentSearch.removeFromSearchHistory(recentSearch.id);
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: qParams,
      queryParamsHandling: 'merge',
    });
  }

  removeRecentSearchQueryParam(id: number) {
    this.recentSearch.removeFromSearchHistory(id);
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => subscription.unsubscribe());
  }
}
