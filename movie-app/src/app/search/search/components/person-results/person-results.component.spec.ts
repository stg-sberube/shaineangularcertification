import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatPaginatorModule, MatTableModule, MatTooltipModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { SearchService } from '@shared/services/search.service';
import { SharedModule } from '@shared/shared.module';

import { PersonResultsComponent } from './person-results.component';

describe('PersonResultsComponent', () => {
  let component: PersonResultsComponent;
  let fixture: ComponentFixture<PersonResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PersonResultsComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatTooltipModule,
        MatPaginatorModule,
        SharedModule,
      ],
      providers: [SearchService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
