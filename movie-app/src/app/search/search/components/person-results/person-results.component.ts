import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonsPage } from '@shared/interfaces/persons-page';
import { PersonsResult } from '@shared/interfaces/persons-result';
import { AuthService } from '@shared/services/auth.service';
import { SearchService } from '@shared/services/search.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-person-results',
  templateUrl: './person-results.component.html',
  styleUrls: ['./person-results.component.scss'],
})
export class PersonResultsComponent implements OnDestroy {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  results$: Observable<PersonsResult[]> = this.search.personResults$;
  page$: Observable<PersonsPage> = this.search.personData$;
  displayedColumns: string[] = [];
  subs: Subscription[] = [];
  constructor(
    private search: SearchService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService
  ) {
    const temp = this.auth.isAuthenticated$.subscribe(authed => {
      if (authed) {
        this.displayedColumns = ['name', 'known_for', 'popularity', 'actions'];
      } else {
        this.displayedColumns = ['name', 'known_for', 'popularity'];
      }
    });
    this.subs.push(temp);
  }

  shortenString(str: string): string {
    if (str.length > 30) {
      const newStr = str.substr(0, 30);
      if (newStr.substr(-1) === ' ') {
        return newStr.trim() + '...';
      } else {
        return newStr + '...';
      }
    } else {
      if (str.length > 1) {
        return str;
      } else {
        return 'No overview provided...';
      }
    }
  }

  paginate(event: PageEvent) {
    const route = this.route.queryParams.subscribe(params => {
      if (params.query) {
        this.search.search('person', params.query, null, null, event.pageIndex + 1);
      }
    });
    this.subs.push(route);
  }

  goToDetails(row) {
    const route = this.route.queryParams.subscribe(params => {
      this.router.navigate(['details', 'person', row.id], {
        state: { data: { comingFrom: 'search', params } },
      });
    });
    this.subs.push(route);
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => {
      subscription.unsubscribe();
    });
    this.search.resetPersonResults();
  }
}
