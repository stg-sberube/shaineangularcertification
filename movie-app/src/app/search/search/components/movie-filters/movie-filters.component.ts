import { Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchType } from '@shared/interfaces/search-type';
import { SearchService } from '@shared/services/search.service';
import { Subscription } from 'rxjs';
import { RecentSearchHistoryService } from 'src/app/search/recent-search-history.service';

@Component({
  selector: 'app-movie-filters',
  templateUrl: './movie-filters.component.html',
  styleUrls: ['./movie-filters.component.scss'],
})
export class MovieFiltersComponent implements OnDestroy {
  formGroup: FormGroup;
  years: number[] = [];
  compareFn: ((f1: number, f2: number) => boolean) | null = this.compareByValue;
  subs: Subscription[] = [];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private search: SearchService,
    private recentSearches: RecentSearchHistoryService
  ) {
    const currentYear: number = new Date().getFullYear();
    const minYear: number = 1902;
    let counter = currentYear;
    /* #loop This while loop builds our year select dropdown, I could probably shorten the code though. */
    while (counter >= minYear) {
      this.years.push(counter);
      counter = counter - 1;
    }
    this.route.queryParams.subscribe(params => {
      // #form Creating this based on passed in query params, this allows the query params to be put in as default values in the form
      this.formGroup = new FormGroup({
        /* #validation I only wanted the movie or Tv radio button to be required and the query,
        because we call different API endpoints depending on which radio is selected */
        movieTV: new FormControl(params.mOrT ? params.mOrT : null, Validators.required),
        query: new FormControl(params.query ? params.query : null, Validators.required),
        year: new FormControl(params.year ? params.year : null),
      });
      if (params.mOrT && params.query) {
        this.search.search(
          'movie',
          params.query,
          params.mOrT ? params.mOrT : 'movie',
          params.year ? params.year : null
        );
      }
    });
  }

  submit() {
    // #form Getting values from the form
    const mOrT = this.formGroup.get('movieTV').value;
    const query = this.formGroup.get('query').value;
    const year = this.formGroup.get('year').value ? this.formGroup.get('year').value : null;
    // #service Now we send form values to the service and thence to http to call the API
    this.search.search('movie', query, mOrT ? mOrT : 'movie', year ? year : null);
    /* #service The call above actually sets an observable, so our work here is done,
    except for getting the query parameters and doing a few extra things with them */
    const qParams = this.search.getQueryParams({ query, year, mOrT });
    // #service Sending query params off to the session to be stored
    this.recentSearches.addToSearchHistory(mOrT === 'movie' ? SearchType.Movie : SearchType.TV, qParams);
    // #http Now we're resetting the url with the query params
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: qParams,
      queryParamsHandling: 'merge',
    });
  }

  /* This function was a complex pain in the butt to figure out, basically put we want the
  function to return true when it matches the element that is selected, in this case we can
  either compare numbers directly or as strings.

  Also note, this is the only way to set Material's Select Dropdown visually, you can set the
  value and the form will recognize that the value is set, but unless you do this you will not
  visually see that it is set. */
  compareByValue(f1: number, f2: number) {
    return f1 && f2 && f1.toString() === f2.toString();
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => subscription.unsubscribe());
  }
}
