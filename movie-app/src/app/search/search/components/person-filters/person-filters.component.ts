import { Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchType } from '@shared/interfaces/search-type';
import { SearchService } from '@shared/services/search.service';
import { Subscription } from 'rxjs';
import { RecentSearchHistoryService } from 'src/app/search/recent-search-history.service';

@Component({
  selector: 'app-person-filters',
  templateUrl: './person-filters.component.html',
  styleUrls: ['./person-filters.component.scss'],
})
export class PersonFiltersComponent implements OnDestroy {
  formGroup: FormGroup;
  subs: Subscription[] = [];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private search: SearchService,
    private recentSearches: RecentSearchHistoryService
  ) {
    this.route.queryParams.subscribe(params => {
      this.formGroup = new FormGroup({
        query: new FormControl(params.query ? params.query : null, Validators.required),
      });
      if (params.query) {
        this.search.search('person', params.query);
      }
    });
  }

  submit() {
    const query = this.formGroup.get('query').value;
    this.search.search('movie', query);
    const qParams = this.search.getQueryParams({ query });
    this.recentSearches.addToSearchHistory(SearchType.Person, qParams);
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: qParams,
      queryParamsHandling: 'merge',
    });
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => subscription.unsubscribe());
  }
}
