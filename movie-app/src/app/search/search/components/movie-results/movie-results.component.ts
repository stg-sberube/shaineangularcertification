import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { MoviesPage } from '@shared/interfaces/movies-page';
import { MoviesResult } from '@shared/interfaces/movies-result';
import { AuthService } from '@shared/services/auth.service';
import { SearchService } from '@shared/services/search.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-movie-results',
  templateUrl: './movie-results.component.html',
  styleUrls: ['./movie-results.component.scss'],
})
export class MovieResultsComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  results$: Observable<MoviesResult[]> = this.search.movieResults$;
  page$: Observable<MoviesPage> = this.search.movieData$;
  displayedColumns: string[] = [];
  subs: Subscription[] = [];
  constructor(
    private search: SearchService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService
  ) {}

  ngOnInit() {
    const route = this.route.queryParams.subscribe(params => {
      if (params.mOrT && params.mOrT === 'movie') {
        if (this.auth.isAuthenticated()) {
          this.displayedColumns = ['title', 'release_date', 'overview', 'popularity', 'actions'];
        } else {
          this.displayedColumns = ['title', 'release_date', 'overview', 'popularity'];
        }
      } else if (params.mOrT && params.mOrT === 'tv') {
        if (this.auth.isAuthenticated()) {
          this.displayedColumns = ['name', 'first_air_date', 'overview', 'popularity', 'actions'];
        } else {
          this.displayedColumns = ['name', 'first_air_date', 'overview', 'popularity'];
        }
      }
    });
    this.subs.push(route);
  }

  shortenString(str: string): string {
    if (str.length > 30) {
      const newStr = str.substr(0, 30);
      if (newStr.substr(-1) === ' ') {
        return newStr.trim() + '...';
      } else {
        return newStr + '...';
      }
    } else {
      if (str.length > 1) {
        return str;
      } else {
        return 'No overview provided...';
      }
    }
  }

  paginate(event: PageEvent) {
    const route = this.route.queryParams.subscribe(params => {
      if (params.mOrT && params.query) {
        this.search.search(
          'movie',
          params.query,
          params.mOrT ? params.mOrT : 'movie',
          params.year ? params.year : null,
          event.pageIndex + 1
        );
      }
    });
    this.subs.push(route);
  }

  goToDetails(row) {
    const route = this.route.queryParams.subscribe(params => {
      if (params.mOrT === 'movie') {
        this.router.navigate(['details', 'movie', row.id], {
          state: { data: { comingFrom: 'search', params } },
        });
      } else {
        this.router.navigate(['details', 'tv', row.id], {
          state: { data: { comingFrom: 'search', params } },
        });
      }
    });
    this.subs.push(route);
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => {
      subscription.unsubscribe();
    });
    this.search.resetMovieResults();
  }
}
