import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatPaginatorModule, MatTableModule, MatTooltipModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { SearchService } from '@shared/services/search.service';
import { SharedModule } from '@shared/shared.module';

import { MovieResultsComponent } from './movie-results.component';

describe('MovieResultsComponent', () => {
  let component: MovieResultsComponent;
  let fixture: ComponentFixture<MovieResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MovieResultsComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatTooltipModule,
        MatPaginatorModule,
        SharedModule,
      ],
      providers: [SearchService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
