import { TestBed } from '@angular/core/testing';

import { RecentSearchHistoryService } from './recent-search-history.service';

describe('RecentSearchHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecentSearchHistoryService = TestBed.get(RecentSearchHistoryService);
    expect(service).toBeTruthy();
  });
});
