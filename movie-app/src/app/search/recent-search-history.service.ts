import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { SearchType } from '@shared/interfaces/search-type';
import { SessionRecentSearch } from '@shared/interfaces/session-recent-search';
import { ClientStorageService } from '@shared/services/client-storage.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RecentSearchHistoryService {
  private searchHistory: BehaviorSubject<SessionRecentSearch[]> = new BehaviorSubject<SessionRecentSearch[]>(
    this.getRecentSearchHistory()
  );
  searchHistory$: Observable<SessionRecentSearch[]> = this.searchHistory.asObservable();
  movieSearchHistory$: Observable<SessionRecentSearch[]> = this.searchHistory$.pipe(
    map(searches => searches.filter(s => s.type === SearchType.Movie))
  );
  tvSearchHistory$: Observable<SessionRecentSearch[]> = this.searchHistory$.pipe(
    map(searches => searches.filter(s => s.type === SearchType.TV))
  );
  personSearchHistory$: Observable<SessionRecentSearch[]> = this.searchHistory$.pipe(
    map(searches => searches.filter(s => s.type === SearchType.Person))
  );
  constructor(private storage: ClientStorageService) {}

  getRecentSearchHistory(): SessionRecentSearch[] {
    return this.storage.getSession().recentSearches;
  }

  getHistoryByType(type: SearchType): SessionRecentSearch[] {
    const history = this.getRecentSearchHistory();
    const returnHistory: SessionRecentSearch[] = [];
    history.forEach(item => {
      if (item.type === type) {
        returnHistory.push(item);
      }
    });
    return returnHistory;
  }

  addToSearchHistory(type: SearchType, params: Params): boolean {
    const history = this.getRecentSearchHistory();
    if (history.length > 0 && !this.checkDuplicates(type, params)) {
      const newHistoryId = this.generateNextId(); // Just in case, do this before reversing array
      const newHistory = history.reverse(); // Put most recent at the bottom
      newHistory.push({ id: newHistoryId, type, params, date: new Date() }); // Add onto end of array
      newHistory.reverse(); // Put most recent at the top
      this.searchHistory.next(newHistory);
      return this.storage.updateSession('recentSearches', newHistory);
    } else if (this.checkDuplicates(type, params)) {
      // If we have an entry with our exact query match, remove old and re-add it
      const dupId = this.getDuplicate(type, params);
      this.removeFromSearchHistory(dupId);
      return this.addToSearchHistory(type, params);
    } else {
      history.push({ id: 1, type, params, date: new Date() });
      this.searchHistory.next(history);
      return this.storage.updateSession('recentSearches', history);
    }
  }

  checkDuplicates(type: SearchType, params: Params): boolean {
    let returnBoolean = false;
    const history = this.getRecentSearchHistory();
    history.forEach(search => {
      if (search.type === type) {
        for (const [key, value] of Object.entries(search.params)) {
          if (key === 'query' && value === params[key]) {
            returnBoolean = true;
          }
        }
      }
    });
    return returnBoolean;
  }

  getDuplicate(type: SearchType, params: Params): number {
    let id: number = 0;
    const history = this.getRecentSearchHistory();
    history.forEach(search => {
      if (search.type === type) {
        for (const [key, value] of Object.entries(search.params)) {
          if (key === 'query' && value === params[key]) {
            id = search.id;
          }
        }
      }
    });
    return id;
  }

  generateNextId(): number {
    const history = this.getRecentSearchHistory(); // Most recent should be at the top of the array
    return history[0].id + 1;
  }

  removeFromSearchHistory(id: number) {
    const history = this.getRecentSearchHistory();
    const newHistory: SessionRecentSearch[] = [];
    history.forEach(item => {
      if (item.id !== id) {
        newHistory.push(item);
      }
    });
    this.searchHistory.next(newHistory);
    return this.storage.updateSession('recentSearches', newHistory);
  }

  clearSearchHistory() {
    this.searchHistory.next(null);
    return this.storage.updateSession('recentSearches', []);
  }
}
