import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTooltipModule,
} from '@angular/material';
import { SharedModule } from '@shared/shared.module';

import { SearchRoutingModule } from './search-routing.module';
import { MovieFiltersComponent } from './search/components/movie-filters/movie-filters.component';
import { MovieResultsComponent } from './search/components/movie-results/movie-results.component';
import { PersonFiltersComponent } from './search/components/person-filters/person-filters.component';
import { PersonResultsComponent } from './search/components/person-results/person-results.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [
    SearchComponent,
    MovieFiltersComponent,
    MovieResultsComponent,
    PersonFiltersComponent,
    PersonResultsComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatRadioModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatDividerModule,
    MatTooltipModule,
    MatIconModule,
    HttpClientModule,
    SharedModule,
    SearchRoutingModule,
  ],
})
export class SearchModule {}
