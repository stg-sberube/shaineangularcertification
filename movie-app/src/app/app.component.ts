import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { MenuItem } from '@shared/interfaces/menu-item';
import { ClientStorageService } from '@shared/services/client-storage.service';
import { PageSpinnerService } from '@shared/services/page-spinner.service';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';

import { LoginComponent } from './auth/login/login.component';
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  // tslint:disable-next-line
  title = "Shaine's Movies";
  subs: Subscription[] = [];
  private menuItems: BehaviorSubject<MenuItem[]> = new BehaviorSubject<MenuItem[]>(null);
  public menuItems$: Observable<MenuItem[]> = this.menuItems.asObservable();
  private searchItems: BehaviorSubject<MenuItem[]> = new BehaviorSubject<MenuItem[]>(null);
  public searchItems$: Observable<MenuItem[]> = this.searchItems.asObservable();
  authed: boolean;
  constructor(
    public dialog: MatDialog,
    public auth: AuthService,
    private storage: ClientStorageService,
    private router: Router,
    private ps: PageSpinnerService
  ) {
    this.ps.color = 'primary';
    this.ps.diameter = 40;
    this.ps.mode = 'indeterminate';
    this.ps.strokeWidth = 2;
    this.ps.value = 0;
    this.ps.display = false;
  }

  /* #lifecycle-hooks Here we're setting up what the application needs to run properly,
  like the menu and creating the session */
  ngOnInit() {
    this.searchItems.next([
      {
        label: 'Movies or TV',
        content: 'Movies or TV',
        /* #arrow-function You will have to go the interface to see this in action, but
        this is actually an arrow function and it allows me to pass a function in as an
        object property here, this then allows me to call functions written in the
        Typescript and react to them from within the template */
        callback: this.goToSearchMovie.bind(this),
      },
      {
        label: 'Person',
        content: 'Person',
        // #this The binding here is necessary for the function call to work, the function needs context to run.
        callback: this.goToSearchPerson.bind(this),
      },
    ]);
    const temp = this.auth.isAuthenticated$.subscribe(authed => {
      this.authed = authed;
      if (authed) {
        this.menuItems.next([
          {
            label: 'Log Out',
            content: 'Log Out',
            callback: this.logout.bind(this),
          },
        ]);
      } else {
        this.menuItems.next([
          {
            label: 'Log In',
            content: 'Log In',
            callback: this.openSignIn.bind(this),
          },
          {
            label: 'Register',
            content: 'Register',
            callback: this.goToRegister.bind(this),
          },
        ]);
      }
    });
    this.subs.push(temp);
    this.storage.getSession();
  }

  openSignIn(): void {
    const dialogRef = this.dialog.open(LoginComponent, { width: '450px' });
  }

  goToRegister(): void {
    this.router.navigate(['auth', 'register']);
  }

  logout(): void {
    this.auth.logout().subscribe(logout => {
      if (!logout) {
        alert('Error logging out!');
      }
      this.router.navigate([this.router.url]);
    });
  }

  goToSearchMovie(): void {
    this.router.navigate(['search', 'movie']);
  }

  goToSearchPerson(): void {
    this.router.navigate(['search', 'person']);
  }

  ngOnDestroy() {
    this.storage.clearSession();
    this.subs.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
