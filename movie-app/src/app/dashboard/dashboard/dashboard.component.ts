import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MovieCollectionItem } from '@shared/interfaces/movie-collection-item';
import { PersonCollectionItem } from '@shared/interfaces/person-collection-item';
import { SearchType } from '@shared/interfaces/search-type';
import { TvCollectionItem } from '@shared/interfaces/tv-collection-item';
import { CollectionService } from '@shared/services/collection.service';
import { DetailsService } from '@shared/services/details.service';
import { Observable } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  movieCollection$: Observable<MovieCollectionItem[]> = this.collectionService.movieCollection$;
  personCollection$: Observable<PersonCollectionItem[]> = this.collectionService.personCollection$;
  tvCollection$: Observable<TvCollectionItem[]> = this.collectionService.tvCollection$;
  configBaseURL$: Observable<string> = this.detailsService.configuration$.pipe(
    distinctUntilChanged(),
    map(config => config.images.base_url),
    filter(data => data !== null)
  );
  constructor(
    private collectionService: CollectionService,
    private detailsService: DetailsService,
    private router: Router
  ) {}

  shortenString(str: string): string {
    if (str.length > 150) {
      const newStr = str.substr(0, 150);
      if (newStr.substr(-1) === ' ') {
        return newStr.trim() + '...';
      } else {
        return newStr + '...';
      }
    } else {
      if (str.length > 1) {
        return str;
      } else {
        return 'No overview provided...';
      }
    }
  }

  getImageUrl(imagePath: string, baseUrl: string): string {
    if (imagePath && baseUrl) {
      return [baseUrl.replace(/^\/|\/$/g, ''), 'original', imagePath.replace(/^\/|\/$/g, '')].join('/');
    }
    return '';
  }

  goToMovieDetails(id: number) {
    this.router.navigate(['details', 'movie', id]);
  }

  goToTvDetails(id: number) {
    this.router.navigate(['details', 'tv', id]);
  }

  goToPersonDetails(id: number) {
    this.router.navigate(['details', 'person', id]);
  }

  removeMovieFromCollection(id: number) {
    this.collectionService.removeFromCollection(id, SearchType.Movie);
  }

  removeTvFromCollection(id: number) {
    this.collectionService.removeFromCollection(id, SearchType.TV);
  }

  removePersonFromCollection(id: number) {
    this.collectionService.removeFromCollection(id, SearchType.Person);
  }
}
