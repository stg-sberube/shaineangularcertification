import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule, MatCardModule, MatDividerModule, MatGridListModule, MatIconModule } from '@angular/material';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { MovieCollectionItem } from '@shared/interfaces/movie-collection-item';
import { CollectionService } from '@shared/services/collection.service';
import { BehaviorSubject, Observable } from 'rxjs';

import { DashboardComponent } from './dashboard.component';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let service: CollectionServiceMock;
  const testData: MovieCollectionItem[] = [
    {
      id: 351286,
      title: 'Jurassic World: Fallen Kingdom',
      release_date: '2018-06-06',
      tagline: 'The park is gone',
      background_image: '/3s9O5af2xWKWR5JzP2iJZpZeQQg.jpg',
    },
    {
      id: 135397,
      title: 'Jurassic World',
      release_date: '2015-06-06',
      tagline: 'The park is open.',
      background_image: '/t5KONotASgVKq4N19RyhIthWOPG.jpg',
    },
    {
      id: 331,
      title: 'Jurassic Park III',
      release_date: '2001-07-18',
      // tslint:disable-next-line: quotemark
      tagline: "This time, it's not just a walk in the park!",
      background_image: '/5tNgurgAzF3teAziqpX83gaA9d1.jpg',
    },
  ];
  class CollectionServiceMock {
    movieCollection: BehaviorSubject<MovieCollectionItem[]> = new BehaviorSubject<MovieCollectionItem[]>(testData);
    movieCollection$: Observable<MovieCollectionItem[]> = this.movieCollection.asObservable();
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatCardModule,
        MatButtonModule,
        MatIconModule,
        MatGridListModule,
        MatDividerModule,
      ],
      providers: [
        {
          provide: CollectionService,
          useClass: CollectionServiceMock,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(CollectionService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should resolve collection', () => {
    const data = awaitStream(component.movieCollection$);
    expect(data).toEqual(testData);
  });

  it('should have 3 tiles', () => {
    const items = fixture.debugElement.queryAll(By.css('mat-grid-tile.tile'));
    expect(items.length).toBe(3);
  });

  function awaitStream(stream$: Observable<any>) {
    let response = null;
    stream$.subscribe(data => {
      response = data;
    });
    return response;
  }
});
