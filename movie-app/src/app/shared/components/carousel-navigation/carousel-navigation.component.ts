import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CarouselSlide } from '@shared/interfaces/carousel-slide';

@Component({
  selector: 'app-carousel-navigation',
  templateUrl: './carousel-navigation.component.html',
  styleUrls: ['./carousel-navigation.component.scss'],
})
export class CarouselNavigationComponent {
  @Input() slides: CarouselSlide[];
  @Output() goToSlide: EventEmitter<number> = new EventEmitter();
  constructor() {}

  navToSlide(slide: CarouselSlide) {
    this.goToSlide.emit(slide.id);
  }
}
