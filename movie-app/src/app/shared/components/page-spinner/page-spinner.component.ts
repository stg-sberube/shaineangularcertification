import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { PageSpinnerService } from '@shared/services/page-spinner.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-page-spinner',
  templateUrl: './page-spinner.component.html',
  styleUrls: ['./page-spinner.component.scss'],
})
export class PageSpinnerComponent implements OnInit, OnDestroy {
  onShown: EventEmitter<boolean> = new EventEmitter<boolean>();
  onHidden: EventEmitter<boolean> = new EventEmitter<boolean>();
  private subs: Subscription[] = [];
  constructor(public ps: PageSpinnerService) {}

  ngOnInit() {
    const display = this.ps.display$.subscribe(val => {
      if (val) {
        this.onShown.emit(true);
      } else {
        this.onHidden.emit(true);
      }
    });
    this.subs.push(display);
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
