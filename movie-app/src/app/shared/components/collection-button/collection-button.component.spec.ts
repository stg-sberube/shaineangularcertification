import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule, MatIconModule, MatTooltipModule } from '@angular/material';

import { CollectionButtonComponent } from './collection-button.component';

describe('CollectionButtonComponent', () => {
  let component: CollectionButtonComponent;
  let fixture: ComponentFixture<CollectionButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CollectionButtonComponent],
      imports: [HttpClientTestingModule, MatButtonModule, MatTooltipModule, MatIconModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
