import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { MovieCollectionItem } from '@shared/interfaces/movie-collection-item';
import { PersonCollectionItem } from '@shared/interfaces/person-collection-item';
import { SearchType } from '@shared/interfaces/search-type';
import { TvCollectionItem } from '@shared/interfaces/tv-collection-item';
import { AuthService } from '@shared/services/auth.service';
import { CollectionService } from '@shared/services/collection.service';
import { DetailsService } from '@shared/services/details.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-collection-button',
  templateUrl: './collection-button.component.html',
  styleUrls: ['./collection-button.component.scss'],
})
export class CollectionButtonComponent implements OnDestroy {
  @Input() type: SearchType;
  @Input() icon: boolean = false;
  @Input() id: number;
  @Output() clicked = new EventEmitter();
  @Output() results = new EventEmitter();
  authed: boolean;
  subs: Subscription[] = [];
  constructor(
    private collectionService: CollectionService,
    private auth: AuthService,
    private detailsService: DetailsService
  ) {
    const temp = this.auth.isAuthenticated$.subscribe(authed => {
      this.authed = authed;
    });
    this.subs.push(temp);
  }

  addToCollection(event) {
    event.stopPropagation();
    const details = this.detailsService.getDetails(this.id, this.type).subscribe(data => {
      const add = this.collectionService.addToCollection(this.createCollectionItem(data), this.type);
      this.results.emit(add);
    });
    this.subs.push(details);
  }

  inCollection(): boolean {
    return this.collectionService.inCollection(this.id, this.type);
  }

  createCollectionItem(data) {
    if (this.type === SearchType.Movie) {
      return {
        id: data.id,
        title: data.title,
        release_date: data.release_date,
        tagline: data.tagline,
        background_image: data.backdrop_path,
      } as MovieCollectionItem;
    } else if (this.type === SearchType.Person) {
      return {
        id: data.id,
        name: data.name,
        birthday: data.birthday,
        department: data.known_for_department,
        profile_picture: data.profile_path,
      } as PersonCollectionItem;
    } else {
      return {
        id: data.id,
        name: data.name,
        first_air_date: data.first_air_date,
        overview: data.overview,
        background_image: data.backdrop_path,
      } as TvCollectionItem;
    }
  }

  btnClick() {
    this.clicked.emit(true);
  }

  removeFromCollection(event) {
    event.stopPropagation();
    const rm = this.collectionService.removeFromCollection(this.id, this.type);
    this.results.emit(rm);
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => subscription.unsubscribe());
  }
}
