import { Component, Input } from '@angular/core';
import { CarouselOverlay } from '@shared/interfaces/carousel-slide';

@Component({
  selector: 'app-carousel-overlay',
  templateUrl: './carousel-overlay.component.html',
  styleUrls: ['./carousel-overlay.component.scss'],
})
export class CarouselOverlayComponent {
  @Input() overlay: CarouselOverlay = {
    content: '',
    release_date: '',
  };
  constructor() {}
}
