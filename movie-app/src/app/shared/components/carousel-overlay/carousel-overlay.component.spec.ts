import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselOverlayComponent } from './carousel-overlay.component';

describe('CarouselOverlayComponent', () => {
  let component: CarouselOverlayComponent;
  let fixture: ComponentFixture<CarouselOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselOverlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
