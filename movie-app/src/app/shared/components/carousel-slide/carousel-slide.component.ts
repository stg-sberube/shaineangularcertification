import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CarouselSlide } from '@shared/interfaces/carousel-slide';
import { Observable } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { HomeService } from 'src/app/home/home.service';

@Component({
  selector: 'app-carousel-slide',
  templateUrl: './carousel-slide.component.html',
  styleUrls: ['./carousel-slide.component.scss'],
})
export class CarouselSlideComponent {
  @Input() slide: CarouselSlide = {
    id: 0,
    title: '',
    content_image: '',
    order: 0,
    overlay: {
      content: '',
      release_date: '',
    },
  };
  @Output() prev: EventEmitter<boolean> = new EventEmitter();
  @Output() next: EventEmitter<boolean> = new EventEmitter();
  configBaseURL$: Observable<string> = this.homeService.configuration$.pipe(
    distinctUntilChanged(),
    map(config => config.images.base_url),
    filter(data => data !== null)
  );
  constructor(public homeService: HomeService) {}

  goToNext() {
    this.next.emit(true);
  }

  goToPrevious() {
    this.prev.emit(true);
  }
}
