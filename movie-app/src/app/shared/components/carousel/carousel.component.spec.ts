import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material';

import { CarouselNavigationComponent } from '../carousel-navigation/carousel-navigation.component';
import { CarouselOverlayComponent } from '../carousel-overlay/carousel-overlay.component';
import { CarouselSlideComponent } from '../carousel-slide/carousel-slide.component';
import { CarouselTitleComponent } from '../carousel-title/carousel-title.component';
import { CarouselComponent } from './carousel.component';

describe('CarouselComponent', () => {
  let component: CarouselComponent;
  let fixture: ComponentFixture<CarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CarouselComponent,
        CarouselSlideComponent,
        CarouselNavigationComponent,
        CarouselTitleComponent,
        CarouselOverlayComponent,
      ],
      imports: [MatIconModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
