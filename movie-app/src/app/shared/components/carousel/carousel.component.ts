import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { CarouselSlide } from '@shared/interfaces/carousel-slide';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  animations: [
    trigger('fade', [
      state('show', style({ opacity: 1, visibility: 'visible' })),
      state('hide', style({ opacity: 0, visibility: 'hidden' })),
      transition('show <=> hide', [animate(500)]),
    ]),
    // This is cool, but the fade is more natural
    // trigger('fade', [
    //   state('show', style({ transform: 'translateX(0)', opacity: 1, visibility: 'visible' })),
    //   state('hide', style({ transform: 'translateX(-100%)', opacity: 0, visibility: 'hidden' })),
    //   transition('show => hide', [style({ transform: 'translateX(-100%)' }), animate(500)]),
    //   transition('hide => show', [style({ transform: 'translateX(100%)' }), animate(500)]),
    // ]),
  ],
})
export class CarouselComponent implements OnDestroy {
  /* #input Again, wanted this component to be able to be reusable, so passing in objects was necessary. */
  @Input() slides: CarouselSlide[];
  // #input This gives us the currently displayed slide and is what controls the animations for the carousel
  @Input() currentSlide: number;
  /* #output I wanted the parent to be in control of changing slides rather than the component itself,
  this makes the component easier to reuse. */
  @Output() next: EventEmitter<boolean> = new EventEmitter();
  @Output() previous: EventEmitter<boolean> = new EventEmitter();
  @Output() goToSlide: EventEmitter<number> = new EventEmitter();
  /* #interval This allows the carousel to change every 10 seconds, setting this as a scoped variable
  means I can automatically clear it on destroy */
  interval;
  constructor() {
    this.interval = setInterval(() => {
      this.goToNext(true);
    }, 10000);
  }

  goToPrevious(event) {
    this.resetInterval();
    if (event) {
      this.previous.emit(true);
    }
  }

  goToNext(event) {
    this.resetInterval();
    if (event) {
      this.next.emit(true);
    }
  }

  goToClick(event: number) {
    this.resetInterval();
    this.goToSlide.emit(event);
  }

  resetInterval() {
    window.clearInterval(this.interval);
    this.interval = setInterval(() => {
      this.goToNext(true);
    }, 10000);
  }

  ngOnDestroy() {
    window.clearInterval(this.interval);
  }
}
