import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-carousel-title',
  templateUrl: './carousel-title.component.html',
  styleUrls: ['./carousel-title.component.scss'],
})
export class CarouselTitleComponent {
  @Input() title: string;
  constructor() {}
}
