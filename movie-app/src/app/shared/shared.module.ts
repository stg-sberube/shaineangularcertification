import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatIconModule, MatProgressSpinnerModule, MatTooltipModule } from '@angular/material';

import { CarouselNavigationComponent } from './components/carousel-navigation/carousel-navigation.component';
import { CarouselOverlayComponent } from './components/carousel-overlay/carousel-overlay.component';
import { CarouselSlideComponent } from './components/carousel-slide/carousel-slide.component';
import { CarouselTitleComponent } from './components/carousel-title/carousel-title.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { CollectionButtonComponent } from './components/collection-button/collection-button.component';
import { PageSpinnerComponent } from './components/page-spinner/page-spinner.component';
import { TrimPipe } from './pipes/trim.pipe';
import { ClientStorageService } from './services/client-storage.service';
import { EnvironmentService } from './services/environment.service';

@NgModule({
  declarations: [
    PageSpinnerComponent,
    TrimPipe,
    CollectionButtonComponent,
    CarouselComponent,
    CarouselTitleComponent,
    CarouselOverlayComponent,
    CarouselNavigationComponent,
    CarouselSlideComponent,
  ],
  imports: [CommonModule, MatProgressSpinnerModule, MatButtonModule, MatIconModule, MatTooltipModule],
  providers: [ClientStorageService, EnvironmentService],
  exports: [PageSpinnerComponent, TrimPipe, CollectionButtonComponent, CarouselComponent],
})
export class SharedModule {}
