import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SubscriptionsService {
  subs: Subscription[] = [];
  constructor() {}
  addSub(sub: Subscription) {
    this.subs.push(sub);
  }
  unsubscribeAll() {
    this.subs.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
