import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MoviesDetails } from '@shared/interfaces/movies-details';
import { MoviesDetailsConfiguration } from '@shared/interfaces/movies-details-configuration';
import { PersonDetails } from '@shared/interfaces/person-details';
import { PersonDetailsCredits } from '@shared/interfaces/person-details-credits';
import { SearchType } from '@shared/interfaces/search-type';
import { TvDetails } from '@shared/interfaces/tv-details';
import { EnvironmentService } from '@shared/services/environment.service';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DetailsService {
  environment = this.env.getEnvironment();
  movieDetails: BehaviorSubject<MoviesDetails> = new BehaviorSubject<MoviesDetails>(null);
  movieDetails$: Observable<MoviesDetails> = this.movieDetails.asObservable();
  tvDetails: BehaviorSubject<TvDetails> = new BehaviorSubject<TvDetails>(null);
  tvDetails$: Observable<TvDetails> = this.tvDetails.asObservable();
  personDetails: BehaviorSubject<PersonDetails> = new BehaviorSubject<PersonDetails>(null);
  personDetails$: Observable<PersonDetails> = this.personDetails.asObservable();
  configuration$: Observable<MoviesDetailsConfiguration> = this.http
    .get(this.environment.movie_base_url + 'configuration')
    .pipe(map((response: MoviesDetailsConfiguration) => response));
  constructor(private http: HttpClient, private env: EnvironmentService) {}

  getMovieDetails(id: number) {
    this.http
      .get(this.environment.movie_base_url + 'movie/' + id)
      .subscribe((response: MoviesDetails) => this.movieDetails.next(response));
  }

  getTvDetails(id: number) {
    this.http
      .get(this.environment.movie_base_url + 'tv/' + id)
      .subscribe((response: TvDetails) => this.tvDetails.next(response));
  }

  getPersonDetails(id: number) {
    this.http
      .get(this.environment.movie_base_url + 'person/' + id)
      .subscribe((response: PersonDetails) => this.personDetails.next(response));
  }

  getDetails(id: number, type: SearchType): Observable<MoviesDetails | PersonDetails | TvDetails> {
    if (type === SearchType.Movie) {
      return this.http
        .get(this.environment.movie_base_url + 'movie/' + id)
        .pipe(map((response: MoviesDetails) => response));
    } else if (type === SearchType.Person) {
      return this.http
        .get(this.environment.movie_base_url + 'person/' + id)
        .pipe(map((response: PersonDetails) => response));
    } else {
      return this.http.get(this.environment.movie_base_url + 'tv/' + id).pipe(map((response: TvDetails) => response));
    }
  }

  getMoviePosterURL() {
    return combineLatest([this.movieDetails$, this.configuration$]).pipe(
      map(([details, configuration]) => {
        // return original size of images and use CSS to adjust it's size.
        if (details && details.poster_path) {
          return [
            configuration.images.base_url.replace(/^\/|\/$/g, ''),
            'original',
            details.poster_path.replace(/^\/|\/$/g, ''),
          ].join('/');
        }
      })
    );
  }

  getMovieBGImageURL(imagePath?: string) {
    if (!imagePath) {
      return combineLatest([this.movieDetails$, this.configuration$]).pipe(
        map(([details, configuration]) => {
          if (details && details.backdrop_path) {
            return [
              configuration.images.base_url.replace(/^\/|\/$/g, ''),
              'original',
              details.backdrop_path.replace(/^\/|\/$/g, ''),
            ].join('/');
          }
        })
      );
    } else {
      return combineLatest([of(imagePath), this.configuration$]).pipe(
        map(([details, configuration]) => {
          return [
            configuration.images.base_url.replace(/^\/|\/$/g, ''),
            'original',
            details.replace(/^\/|\/$/g, ''),
          ].join('/');
        })
      );
    }
  }

  getTvPosterURL() {
    return combineLatest([this.tvDetails$, this.configuration$]).pipe(
      map(([details, configuration]) => {
        // return original size of images and use CSS to adjust it's size.
        if (details && details.poster_path) {
          return [
            configuration.images.base_url.replace(/^\/|\/$/g, ''),
            'original',
            details.poster_path.replace(/^\/|\/$/g, ''),
          ].join('/');
        }
      })
    );
  }

  getTvBGImageURL(imagePath?: string) {
    if (!imagePath) {
      return combineLatest([this.tvDetails$, this.configuration$]).pipe(
        map(([details, configuration]) => {
          if (details && details.backdrop_path) {
            return [
              configuration.images.base_url.replace(/^\/|\/$/g, ''),
              'original',
              details.backdrop_path.replace(/^\/|\/$/g, ''),
            ].join('/');
          }
        })
      );
    } else {
      return combineLatest([of(imagePath), this.configuration$]).pipe(
        map(([details, configuration]) => {
          return [
            configuration.images.base_url.replace(/^\/|\/$/g, ''),
            'original',
            details.replace(/^\/|\/$/g, ''),
          ].join('/');
        })
      );
    }
  }

  getProfileURL() {
    return combineLatest([this.personDetails$, this.configuration$]).pipe(
      map(([details, configuration]) => {
        if (details && details.profile_path) {
          return [
            configuration.images.base_url.replace(/^\/|\/$/g, ''),
            'original',
            details.profile_path.replace(/^\/|\/$/g, ''),
          ].join('/');
        }
      })
    );
  }

  getPersonCredits() {
    return this.personDetails$.pipe(
      filter(d => d !== null),
      switchMap(details => {
        return this.http
          .get(this.environment.movie_base_url + 'person/' + details.id + '/combined_credits')
          .pipe(map((response: PersonDetailsCredits) => response));
      })
    );
  }
}
