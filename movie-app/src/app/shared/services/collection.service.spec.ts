import { MovieCollectionItem } from '@shared/interfaces/movie-collection-item';
import { SearchType } from '@shared/interfaces/search-type';
import { ClientStorageService } from '@shared/services/client-storage.service';

import { CollectionService } from './collection.service';

describe('CollectionService', () => {
  let service: CollectionService;
  let storage: ClientStorageService;
  const movie1: MovieCollectionItem = {
    id: 351286,
    title: 'Jurassic World: Fallen Kingdom',
    release_date: '2018-06-06',
    tagline: 'The park is gone',
    background_image: '/3s9O5af2xWKWR5JzP2iJZpZeQQg.jpg',
  } as MovieCollectionItem;
  const movie2: MovieCollectionItem = {
    id: 135397,
    title: 'Jurassic World',
    release_date: '2015-06-06',
    tagline: 'The park is open.',
    background_image: '/t5KONotASgVKq4N19RyhIthWOPG.jpg',
  } as MovieCollectionItem;
  const movie3: MovieCollectionItem = {
    id: 331,
    title: 'Jurassic Park III',
    release_date: '2001-07-18',
    // tslint:disable-next-line: quotemark
    tagline: "This time, it's not just a walk in the park!",
    background_image: '/5tNgurgAzF3teAziqpX83gaA9d1.jpg',
  } as MovieCollectionItem;
  const unsortedCollection: MovieCollectionItem[] = [
    {
      id: 135397,
      title: 'Jurassic World',
      release_date: '2015-06-06',
      tagline: 'The park is open.',
      background_image: '/t5KONotASgVKq4N19RyhIthWOPG.jpg',
    },
    {
      id: 351286,
      title: 'Jurassic World: Fallen Kingdom',
      release_date: '2018-06-06',
      tagline: 'The park is gone',
      background_image: '/3s9O5af2xWKWR5JzP2iJZpZeQQg.jpg',
    },
  ];
  const sortedCollection: MovieCollectionItem[] = [
    {
      id: 351286,
      title: 'Jurassic World: Fallen Kingdom',
      release_date: '2018-06-06',
      tagline: 'The park is gone',
      background_image: '/3s9O5af2xWKWR5JzP2iJZpZeQQg.jpg',
    },
    {
      id: 135397,
      title: 'Jurassic World',
      release_date: '2015-06-06',
      tagline: 'The park is open.',
      background_image: '/t5KONotASgVKq4N19RyhIthWOPG.jpg',
    },
  ];
  const addedCollection: MovieCollectionItem[] = [
    {
      id: 351286,
      title: 'Jurassic World: Fallen Kingdom',
      release_date: '2018-06-06',
      tagline: 'The park is gone',
      background_image: '/3s9O5af2xWKWR5JzP2iJZpZeQQg.jpg',
    },
    {
      id: 135397,
      title: 'Jurassic World',
      release_date: '2015-06-06',
      tagline: 'The park is open.',
      background_image: '/t5KONotASgVKq4N19RyhIthWOPG.jpg',
    },
    {
      id: 331,
      title: 'Jurassic Park III',
      release_date: '2001-07-18',
      // tslint:disable-next-line: quotemark
      tagline: "This time, it's not just a walk in the park!",
      background_image: '/5tNgurgAzF3teAziqpX83gaA9d1.jpg',
    },
  ];
  const movieId1: number = 351286;
  const movieId2: number = 105;

  beforeEach(() => {
    storage = new ClientStorageService();
    service = new CollectionService(storage);
    service.addToCollection(movie1, SearchType.Movie);
    service.addToCollection(movie2, SearchType.Movie);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /* #arrow-functions Really useful in the test bed here, in this test we add items to the
  session collection, verify they are there, and then remove them */
  // #lint-rule tslint doesn't like double quotes, but that's the only way to do this particular string, so disable rule on next line
  // tslint:disable-next-line: quotemark
  it("#addToCollection should add provided movie object to user's collection", () => {
    expect(service.addToCollection(movie3, SearchType.Movie)).toBeTruthy();
    expect(service.getMovieCollection()).toEqual(addedCollection);
    expect(service.removeFromCollection(movie3.id, SearchType.Movie)).toBeTruthy();
  });
  // tslint:disable-next-line: quotemark
  it("#inCollection should return true if movie is in user's collection or false if not", () => {
    expect(service.inCollection(movieId1, SearchType.Movie)).toEqual(true);
    expect(service.inCollection(movieId2, SearchType.Movie)).toEqual(false);
  });
  // tslint:disable-next-line: quotemark
  it("#getCollection should return user's collection", () => {
    // service.addToCollection(movie2);
    expect(service.getMovieCollection()).toEqual(sortedCollection);
  });
  it('#sortCollection should return provided collection in order of release date', () => {
    expect(service.movieSort(unsortedCollection)).toEqual(sortedCollection);
  });
  // tslint:disable-next-line: quotemark
  it("#removeFromCollection should remove movie object by id from user's collection", () => {
    expect(service.removeFromCollection(movie1.id, SearchType.Movie)).toBeTruthy();
    expect(service.removeFromCollection(movie2.id, SearchType.Movie)).toBeTruthy();
    expect(service.getMovieCollection()).toEqual([]);
  });
});
