import { Injectable } from '@angular/core';
import { ProgressSpinnerMode, ThemePalette } from '@angular/material';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PageSpinnerService {
  private _color: BehaviorSubject<ThemePalette> = new BehaviorSubject<ThemePalette>(null);
  public color$: Observable<ThemePalette> = this._color.asObservable();
  private _diameter: BehaviorSubject<number> = new BehaviorSubject<number>(null);
  public diameter$: Observable<number> = this._diameter.asObservable();
  private _mode: BehaviorSubject<ProgressSpinnerMode> = new BehaviorSubject<ProgressSpinnerMode>(null);
  public mode$: Observable<ProgressSpinnerMode> = this._mode.asObservable();
  private _strokeWidth: BehaviorSubject<number> = new BehaviorSubject<number>(null);
  public strokeWidth$: Observable<number> = this._strokeWidth.asObservable();
  private _value: BehaviorSubject<number> = new BehaviorSubject<number>(null);
  public value$: Observable<number> = this._value.asObservable();
  private _display: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  public display$: Observable<boolean> = this._display.asObservable();
  set display(value) {
    this._display.next(value);
  }
  get display(): boolean {
    return this._display.getValue();
  }
  set color(value: ThemePalette) {
    this._color.next(value);
  }
  set diameter(value: number) {
    this._diameter.next(value);
  }
  set mode(value: ProgressSpinnerMode) {
    this._mode.next(value);
  }
  set strokeWidth(value: number) {
    this._strokeWidth.next(value);
  }
  set value(value: number) {
    this._value.next(value);
  }
  get value(): number {
    return this._value.getValue();
  }
  constructor() {}
}
