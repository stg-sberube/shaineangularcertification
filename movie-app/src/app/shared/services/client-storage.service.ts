import { Injectable } from '@angular/core';
import { makeSession, Session } from '@shared/interfaces/session';
import { User } from '@shared/interfaces/user';

@Injectable({
  providedIn: 'root',
})
export class ClientStorageService {
  clientStorage = window.localStorage;

  createSession(): boolean {
    const session = makeSession();
    return this.setItem('session', JSON.stringify(session));
  }

  updateSession(key: string, value: any): boolean {
    const session: Session = this.getSession();
    session[key] = value;
    return this.setItem('session', JSON.stringify(session));
  }

  getSession(): Session {
    let session = this.getItem('session');
    if (!session) {
      this.createSession();
      session = this.getItem('session');
    }
    return JSON.parse(session as string) as Session;
  }

  clearSession(): boolean {
    return this.removeItem('session');
  }

  setSessionUser(userId: number) {
    const session: Session = this.getSession();
    if (userId > 0) {
      session.userId = userId;
      session.authed = true;
    } else {
      session.userId = 0;
      session.authed = false;
    }
    return this.setItem('session', JSON.stringify(session));
  }

  addUser(user: User): boolean {
    const users = this.getUsers();
    for (const u of users) {
      if (user.id === u.id) {
        // We shouldn't be using add user for an existing user
        return false;
      } else if (user.username === u.username || user.email === u.email) {
        // No duplicates
        return false;
      }
    }
    // #spread-operator Here we grab whatever users we currently have and add a new user to the session
    const newUsers = [...users, user];
    return this.setItem('users', JSON.stringify(newUsers));
  }

  getLatestUserId(): number {
    let latestId = 0;
    const users = this.getUsers();
    for (const u of users) {
      if (u.id > latestId) {
        latestId = u.id;
      }
    }
    return latestId;
  }

  updateUser(user: User, update: User): boolean {
    const users = this.getUsers();
    const newUsers: User[] = [];
    let updated = false;
    for (const u of users) {
      if (user.id === u.id) {
        newUsers.push(update);
        updated = true;
      } else {
        newUsers.push(u);
      }
    }
    updated = this.setItem('users', JSON.stringify(newUsers));
    return updated;
  }

  getUsers(): User[] {
    const users = this.getItem('users');
    if (!users) {
      return [] as User[];
    } else {
      return JSON.parse(users as string) as User[];
    }
  }

  getUser(id: number): User {
    const users = this.getUsers();
    for (const u of users) {
      if (id === u.id) {
        return u;
      }
    }
    return {} as User;
  }

  getUserByUsername(username: string): User {
    const users = this.getUsers();
    for (const u of users) {
      if (username === u.username) {
        return u;
      }
    }
    return {} as User;
  }

  removeUser(user: User): boolean {
    const users = this.getUsers();
    const newUsers: User[] = [];
    for (const u of users) {
      if (user.id !== u.id) {
        newUsers.push(u);
      }
    }
    return this.setItem('users', JSON.stringify(newUsers));
  }

  clearUsers(): boolean {
    return this.removeItem('users');
  }

  setItem(key: string, value: any): boolean {
    try {
      this.clientStorage.setItem(key, typeof value === 'string' ? value : value.toString());
    } catch (e) {
      return false;
    }
    return true;
  }

  getItem(key: string): string | boolean {
    try {
      return this.clientStorage.getItem(key);
    } catch (e) {
      return false;
    }
  }

  removeItem(key: string): boolean {
    try {
      this.clientStorage.removeItem(key);
      return true;
    } catch (e) {
      return false;
    }
  }

  resetStorage(): boolean {
    try {
      this.clientStorage.clear();
      return true;
    } catch (e) {
      return false;
    }
  }
}
