import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MoviesPage } from '@shared/interfaces/movies-page';
import { MoviesResult } from '@shared/interfaces/movies-result';
import { ClientStorageService } from '@shared/services/client-storage.service';
import { EnvironmentService } from '@shared/services/environment.service';
import { filter } from 'rxjs/operators';

import { SearchService } from './search.service';

describe('SearchService', () => {
  let injector: TestBed;
  let service: SearchService;
  let httpMock: HttpTestingController;
  let environment: EnvironmentService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [SearchService, EnvironmentService, ClientStorageService],
    });
    injector = getTestBed();
    service = injector.get(SearchService);
    httpMock = injector.get(HttpTestingController);
    environment = injector.get(EnvironmentService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    const search: SearchService = TestBed.get(SearchService);
    expect(search).toBeTruthy();
  });

  describe('#search', () => {
    it('should set pageData$ and pageResults$', () => {
      const env = environment.getEnvironment();
      const dummyPage: MoviesPage = {
        page: 1,
        total_results: 13,
        total_pages: 1,
        results: [
          {
            backdrop_path: '/x4N74cycZvKu5k3KDERJay4ajR3.jpg',
            id: 105,
            original_language: 'en',
            original_title: 'Back to the Future',
            overview:
              "Eighties teenager Marty McFly is accidentally sent back in time to 1955, inadvertently disrupting his parents' first meeting and attracting his mother's romantic interest. Marty must repair the damage to history by rekindling his parents' romance and - with the help of his eccentric inventor friend Doc Brown - return to 1985.",
            popularity: 22.043,
            poster_path: '/pTpxQB1N0waaSc3OSn0e9oc8kx9.jpg',
            release_date: '1985-07-03',
            title: 'Back to the Future',
          },
          {
            backdrop_path: '/amYkOxCwHiVTFKendcIW0rSrRlU.jpg',
            id: 1891,
            original_language: 'en',
            original_title: 'The Empire Strikes Back',
            overview:
              'The epic saga continues as Luke Skywalker, in hopes of defeating the evil Galactic Empire, learns the ways of the Jedi from aging master Yoda. But Darth Vader is more determined than ever to capture Luke. Meanwhile, rebel leader Princess Leia, cocky Han Solo, Chewbacca, and droids C-3PO and R2-D2 are thrown into various stages of capture, betrayal and despair.',
            popularity: 20.008,
            poster_path: '/9SKDSFbaM6LuGqG1aPWN3wYGEyD.jpg',
            release_date: '1980-05-21',
            title: 'The Empire Strikes Back',
          },
          {
            backdrop_path: null,
            id: 390479,
            original_language: 'en',
            original_title: 'Flash Back',
            overview: "Life flashes before a soldier's eyes in his moment of death",
            popularity: 0.6,
            poster_path: '/iByiSe8QvVD57qjY259blkPFFF6.jpg',
            release_date: '1985-03-30',
            title: 'Flash Back',
          },
          {
            backdrop_path: null,
            id: 567122,
            original_language: 'fr',
            original_title: 'Flash Back',
            overview: '',
            popularity: 0.6,
            poster_path: null,
            release_date: '1985-06-09',
            title: 'Flash Back',
          },
          {
            backdrop_path: null,
            id: 480034,
            original_language: 'en',
            original_title: 'Sleazemania Strikes Back',
            overview:
              "A compilation tape featuring trailers for and footage from low-budget sci-fi and exploitation films. This entry features Forbidden Adventure, Marihuana, The Gorilla Woman, Teenage Cruisers, College Girl Confidential, numerous H.G. Lewis and Ed Wood films and others. It also includes test footage from Fred Olen Ray's unfinished Beach Blanket Bloodbath project, which was based on a script (again, unfinished) written by Wood shortly before his death.",
            popularity: 0.6,
            poster_path: '/urqyGW63YCCxkl8TYplklpPj0iu.jpg',
            release_date: '1985-01-01',
            title: 'Sleazemania Strikes Back',
          },
          {
            backdrop_path: '/d7Aax0fvHLM0Gq8Kpm1Kec6518v.jpg',
            id: 294145,
            original_language: 'en',
            original_title: 'Looking Back to the Future: Raymond Loewy, Industrial Designer',
            overview: '',
            popularity: 0.6,
            poster_path: '/xFuQfuPgvMgc6x17lRGK49CmPpP.jpg',
            release_date: '1985-01-01',
            title: 'Looking Back to the Future: Raymond Loewy, Industrial Designer',
          },
          {
            backdrop_path: '/wvyfN89KJizwhaMzmILeWYiC8Hx.jpg',
            id: 75842,
            original_language: 'en',
            original_title: '教頭發威',
            overview:
              "In The Master Strikes Back, Hong Kong's Steadicam pioneering director Sun Chung brings back legendary Ti Lung to play famous weapons instructor Tung Tieh-cheng, who is invited to teach a Ching official's soldiers, in this unofficial sequel to The Kung-fu Instructor. But after his son is kidnapped and castrated what follows is a chaotic, human whirlwind of slicing and dicing compliments of the highly touted martial arts director and one of Jackie Chan's kung-fu classmates, Yuan Te.",
            popularity: 0.6,
            poster_path: '/8Zz1UqipLiVS01phuFt7QGuFigO.jpg',
            release_date: '1985-01-19',
            title: 'The Master Strikes Back',
          },
          {
            backdrop_path: null,
            id: 230364,
            original_language: 'en',
            original_title: 'The Making of Back to the Future',
            overview: "The Making of 'Back to the Future'",
            popularity: 0.6,
            poster_path: '/i4kowl4CAR7SukWU0AUaqMscMSf.jpg',
            release_date: '1985-10-25',
            title: 'The Making of Back to the Future',
          },
          {
            backdrop_path: null,
            id: 278229,
            original_language: 'en',
            original_title: 'Only Fools and Horses - To Hull and Back',
            overview:
              "Boycie and Abdul pitch a diamond scam to Del Boy, who immediately turns them down. That is until they offer him a £15,000 cut of the estimated £150,000 sale of the stone on the UK market. Del finds himself designated as the courier between Holland and Britain. No sooner has Del enlisted a reluctant Rodders, he hears his old foil Chief Inspector Slater is eyeing Boycie and Abdul as drug dealers. Del decides to hide undetected in the back of Denzil's van. Denzil then getting in and driving them to Hull (pursued by Rodney) was not part of the plan. Thinking quickly, they hire a boat, and let Uncle Albert guide them to Amsterdam. Overcoming counterfeit cash, Albert's amnesia, it's only the arrival of Slater that scuppers them. Despite this, it's Del who has the last laugh.",
            popularity: 0.6,
            poster_path: '/oRpY2ZD7xnKYQqNjYBOwuhhVPo1.jpg',
            release_date: '1985-12-25',
            title: 'Only Fools and Horses - To Hull and Back',
          },
          {
            backdrop_path: '/sekwyAcwibYm8Bz2h60vzFGRBow.jpg',
            id: 175966,
            original_language: 'vi',
            original_title: 'Bao giờ cho đến tháng mười',
            overview:
              'Considered by many local and international critics to be the greatest Vietnamese movie ever made. In the final days of the war, a beautiful young widow, Duyen, faces a daily struggle to take care of her young son and ailing father-in-law, all the while hiding from them the fact that her husband has recently been killed in battle. Keeping her secret burden to herself, she is befriended by the village schoolmaster, Zhang, who agrees to fabricate letters from her dead husband in order to spare her family sorrow. As their friendship deepens, Duyen and Zhang find themselves drawn closer to intimacy – a dangerous relationship if Duyen if to maintain her charade. The film resonates beautifully with the traditional Vietnamese precepts of duty and sacrifice, combined with aesthetic influences from centuries of tradional poetry, literature and theatre.',
            popularity: 1.16,
            poster_path: '/5dBeb1Pffq2Ur3PgFJNumC5VB1Y.jpg',
            release_date: '1984-01-01',
            title: 'When the Tenth Month Comes',
          },
          {
            backdrop_path: '/mHg9Xym9KVmIjsUu3wHtXvZWGxB.jpg',
            id: 79420,
            original_language: 'fr',
            original_title: 'Partir, revenir',
            overview: '',
            popularity: 2.559,
            poster_path: '/b6tNkv2LQDW2MLcg5AXEasSdxhB.jpg',
            release_date: '1985-10-01',
            title: 'Partir, revenir',
          },
          {
            backdrop_path: null,
            id: 381944,
            original_language: 'de',
            original_title: 'Backfischliebe',
            overview: '',
            popularity: 0.6,
            poster_path: null,
            release_date: '1985-11-20',
            title: 'Backfischliebe',
          },
          {
            backdrop_path: null,
            id: 209574,
            original_language: 'en',
            original_title: 'American Commandos',
            overview:
              'A group of Vietnam vets are sent to Southeast Asia to destroy drug-smuggling operations in the Golden Triangle. When they get there, they find that many of the drug gangs are run by other Vietnam vets.',
            popularity: 0.65,
            poster_path: '/p68yDuWEytTQ2dPCs3HeICxqJh4.jpg',
            release_date: '1985-11-11',
            title: 'American Commandos',
          },
        ],
      };
      const dummyResults: MoviesResult[] = dummyPage.results;
      service.search('movie', 'back', 'movie', 1985);
      service.movieData$.pipe(filter(d => d !== null)).subscribe(data => {
        expect(data).toEqual(dummyPage);
      });
      service.movieResults$.pipe(filter(r => r !== null)).subscribe(results => {
        expect(results.length).toBe(13);
        expect(results).toEqual(dummyResults);
      });

      const url = service.createUrl(env.movie_base_url, { query: 'back', year: 1985, page: null }, ['search', 'movie']);
      const req = httpMock.expectOne(url);
      expect(req.request.method).toBe('GET');
      // req.flush(dummyPage);
      // req.flush(dummyResults);
    });
  });
});
