import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router, UrlSerializer } from '@angular/router';
import { MoviesPage } from '@shared/interfaces/movies-page';
import { MoviesResult } from '@shared/interfaces/movies-result';
import { PersonsPage } from '@shared/interfaces/persons-page';
import { PersonsResult } from '@shared/interfaces/persons-result';
import { EnvironmentService } from '@shared/services/environment.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  environment = this.env.getEnvironment();
  private movieResults: BehaviorSubject<MoviesResult[]> = new BehaviorSubject<MoviesResult[]>(null);
  movieResults$: Observable<MoviesResult[]> = this.movieResults.asObservable();
  private movieData: BehaviorSubject<MoviesPage> = new BehaviorSubject<MoviesPage>(null);
  movieData$: Observable<MoviesPage> = this.movieData.asObservable();
  private personResults: BehaviorSubject<PersonsResult[]> = new BehaviorSubject<PersonsResult[]>(null);
  personResults$: Observable<PersonsResult[]> = this.personResults.asObservable();
  private personData: BehaviorSubject<PersonsPage> = new BehaviorSubject<PersonsPage>(null);
  personData$: Observable<PersonsPage> = this.personData.asObservable();
  constructor(
    private http: HttpClient,
    private env: EnvironmentService,
    private router: Router,
    private serializer: UrlSerializer
  ) {}

  search(type: 'movie' | 'person', query: string, mOrT?: 'movie' | 'tv', year?: number, page?: number) {
    let url = '';
    if (type === 'movie') {
      if (mOrT === 'movie') {
        url = this.createUrl(this.environment.movie_base_url, { query, year, page }, ['search', 'movie']);
        this.http.get(url).subscribe((response: MoviesPage) => {
          this.movieData.next(response);
          this.movieResults.next(response.results);
        });
      } else {
        url = this.createUrl(this.environment.movie_base_url, { query, year, page }, ['search', 'tv']);
        this.http.get(url).subscribe((response: MoviesPage) => {
          this.movieData.next(response);
          this.movieResults.next(response.results);
        });
      }
    } else {
      url = this.createUrl(this.environment.movie_base_url, { query, page }, ['search', 'person']);
      this.http.get(url).subscribe((response: PersonsPage) => {
        this.personData.next(response);
        this.personResults.next(response.results);
      });
    }
  }

  createUrl(baseUrl: string, params, additionalRoute?: string[]): string {
    this.environment.movie_api_base_query_params.forEach(param => {
      params[param.key] = param.value;
    });
    const tree = this.router.createUrlTree(additionalRoute ? additionalRoute : [], { queryParams: params });
    let paramString = this.serializer.serialize(tree);
    // check if first two letters in serializer.serialize() is /?, if yes, remove first character so that /? turns into ?
    paramString = paramString.substr(1);
    return baseUrl + paramString;
  }

  getQueryParams(params) {
    this.environment.movie_api_base_query_params.forEach(param => {
      params[param.key] = param.value;
    });
    return params;
  }

  resetMovieResults() {
    this.movieData.next(null);
    this.movieResults.next(null);
  }

  resetPersonResults() {
    this.personData.next(null);
    this.personResults.next(null);
  }
}
