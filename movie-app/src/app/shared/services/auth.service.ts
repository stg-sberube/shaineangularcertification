import { Injectable } from '@angular/core';
import { Session } from '@shared/interfaces/session';
import { User } from '@shared/interfaces/user';
import { ClientStorageService } from '@shared/services/client-storage.service';
import { EnvironmentService } from '@shared/services/environment.service';
import * as CryptoJs from 'crypto-js';
import { BehaviorSubject, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  _isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.isAuthenticated());
  isAuthenticated$: Observable<boolean> = this._isAuthenticated.asObservable();
  constructor(private env: EnvironmentService, private storage: ClientStorageService) {}

  login(username: string, password: string, compare: boolean = true): Observable<User | boolean> {
    const user = this.storage.getUserByUsername(username);
    if (user) {
      if (compare && this.comparePassword(password, user) && username === user.username) {
        this.storage.setSessionUser(user.id);
        this._isAuthenticated.next(true);
        return of(user);
      } else {
        this.storage.setSessionUser(user.id);
        this._isAuthenticated.next(true);
        return of(user);
      }
    }
    return of(false);
  }

  logout(): Observable<boolean> {
    try {
      this.storage.setSessionUser(0);
      this._isAuthenticated.next(false);
      return of(true); // Logged out
    } catch (e) {
      return of(false);
    }
  }

  register(user: User): Observable<boolean | User> {
    user.id = this.storage.getLatestUserId() + 1;
    user.password = this.encryptPassword(user.password);
    const create = this.storage.addUser(user);
    if (!create) {
      return of(false);
    } else {
      return this.login(user.username, user.password, false);
    }
  }

  isUniqueUser(key, value): boolean {
    // Use this for username and email validation on registration form
    const users = this.storage.getUsers();
    for (const user of users) {
      if (user[key] === value) {
        return false;
      }
    }
    return true;
  }

  encryptPassword(password: string) {
    const environment = this.env.getEnvironment();
    const key = CryptoJs.enc.Utf8.parse(environment.encryption_salt);
    const encrypted = CryptoJs.AES.encrypt(CryptoJs.enc.Utf8.parse(password), key, {
      keySize: environment.encryption_keySize,
      iv: key,
      mode: environment.encryption_mode,
      padding: environment.encryption_padding,
    });
    return encrypted.toString();
  }

  comparePassword(password: string, compare: User): boolean {
    // Get user by username, compare with password that is passed in and encrypted.
    // false if no match == false is bad
    const newPass = this.encryptPassword(password);
    if (newPass === compare.password) {
      return true;
    } else {
      return false;
    }
  }

  isAuthenticated(): boolean {
    const session: Session = this.storage.getSession();
    return session.authed;
  }
}
