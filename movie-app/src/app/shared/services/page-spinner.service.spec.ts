import { TestBed } from '@angular/core/testing';

import { PageSpinnerService } from './page-spinner.service';

describe('PageSpinnerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PageSpinnerService = TestBed.get(PageSpinnerService);
    expect(service).toBeTruthy();
  });
});
