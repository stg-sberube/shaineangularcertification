import { Injectable, isDevMode } from '@angular/core';
import { environment as env } from '@env/environment';
import { environment as envProd } from '@env/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class EnvironmentService {
  private _environment = isDevMode() ? env : envProd;
  constructor() {}

  getEnvironment() {
    return this._environment;
  }
}
