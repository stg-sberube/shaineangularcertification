import { Injectable } from '@angular/core';
import { MovieCollectionItem } from '@shared/interfaces/movie-collection-item';
import { PersonCollectionItem } from '@shared/interfaces/person-collection-item';
import { SearchType } from '@shared/interfaces/search-type';
import { TvCollectionItem } from '@shared/interfaces/tv-collection-item';
import { ClientStorageService } from '@shared/services/client-storage.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CollectionService {
  movieCollection: BehaviorSubject<MovieCollectionItem[]> = new BehaviorSubject<MovieCollectionItem[]>(
    this.getMovieCollection()
  );
  movieCollection$: Observable<MovieCollectionItem[]> = this.movieCollection.asObservable();
  personCollection: BehaviorSubject<PersonCollectionItem[]> = new BehaviorSubject<PersonCollectionItem[]>(
    this.getPersonCollection()
  );
  personCollection$: Observable<PersonCollectionItem[]> = this.personCollection.asObservable();
  tvCollection: BehaviorSubject<TvCollectionItem[]> = new BehaviorSubject<TvCollectionItem[]>(this.getTvCollection());
  tvCollection$: Observable<TvCollectionItem[]> = this.tvCollection.asObservable();
  constructor(private storage: ClientStorageService) {}

  addToCollection(item: MovieCollectionItem | PersonCollectionItem | TvCollectionItem, type: SearchType): boolean {
    if (this.inCollection(item.id, SearchType.Movie)) {
      return false;
    }
    let userCollection;
    let sortedCollection;
    if (type === SearchType.Movie) {
      userCollection = this.getMovieCollection();
      userCollection.push(item as MovieCollectionItem);
      sortedCollection = this.movieSort(userCollection);
      this.movieCollection.next(sortedCollection);
      return this.storage.updateSession('userMovieCollection', sortedCollection);
    } else if (type === SearchType.Person) {
      userCollection = this.getPersonCollection();
      userCollection.push(item as PersonCollectionItem);
      sortedCollection = this.personSort(userCollection);
      this.personCollection.next(sortedCollection);
      return this.storage.updateSession('userPersonCollection', sortedCollection);
    } else {
      userCollection = this.getTvCollection();
      userCollection.push(item as TvCollectionItem);
      sortedCollection = this.tvSort(userCollection);
      this.tvCollection.next(sortedCollection);
      return this.storage.updateSession('userTvCollection', sortedCollection);
    }
  }

  removeFromCollection(id: number, type: SearchType): boolean {
    let userCollection;
    if (type === SearchType.Movie) {
      userCollection = this.getMovieCollection();
      const newCollection = userCollection.filter(item => item.id !== id);
      this.movieCollection.next(newCollection);
      return this.storage.updateSession('userMovieCollection', newCollection);
    } else if (type === SearchType.Person) {
      userCollection = this.getPersonCollection();
      const newCollection = userCollection.filter(item => item.id !== id);
      this.personCollection.next(newCollection);
      return this.storage.updateSession('userPersonCollection', newCollection);
    } else {
      userCollection = this.getTvCollection();
      const newCollection = userCollection.filter(item => item.id !== id);
      this.tvCollection.next(newCollection);
      return this.storage.updateSession('userTvCollection', newCollection);
    }
  }

  inCollection(id: number, type: SearchType): boolean {
    let userCollection;
    if (type === SearchType.Movie) {
      userCollection = this.getMovieCollection();
    } else if (type === SearchType.Person) {
      userCollection = this.getPersonCollection();
    } else {
      userCollection = this.getTvCollection();
    }
    let found: boolean = false;
    if (userCollection && userCollection.length > 0) {
      for (const item of userCollection) {
        if (item.id === id) {
          found = true;
          break;
        }
      }
    }
    return found;
  }

  getMovieCollection(): MovieCollectionItem[] {
    return this.movieSort(this.storage.getSession().userMovieCollection);
  }

  getTvCollection(): TvCollectionItem[] {
    return this.tvSort(this.storage.getSession().userTvCollection);
  }

  getPersonCollection(): PersonCollectionItem[] {
    return this.personSort(this.storage.getSession().userPersonCollection);
  }

  movieSort(collection: MovieCollectionItem[]): MovieCollectionItem[] {
    return collection
      .sort((a, b) => {
        const aDate = new Date(a.release_date);
        const bDate = new Date(b.release_date);
        return bDate.getFullYear() - aDate.getFullYear();
      })
      .sort((a, b) => {
        const nameA = a.title.toLowerCase();
        const nameB = b.title.toLowerCase();
        const aDate = new Date(a.release_date).getFullYear();
        const bDate = new Date(b.release_date).getFullYear();
        if (aDate === bDate) {
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        } else {
          return 0;
        }
      });
  }

  tvSort(collection: TvCollectionItem[]): TvCollectionItem[] {
    return collection
      .sort((a, b) => {
        const aDate = new Date(a.first_air_date);
        const bDate = new Date(b.first_air_date);
        return bDate.getFullYear() - aDate.getFullYear();
      })
      .sort((a, b) => {
        const nameA = a.name.toLowerCase();
        const nameB = b.name.toLowerCase();
        const aDate = new Date(a.first_air_date).getFullYear();
        const bDate = new Date(b.first_air_date).getFullYear();
        if (aDate === bDate) {
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        } else {
          return 0;
        }
      });
  }

  personSort(collection: PersonCollectionItem[]): PersonCollectionItem[] {
    return collection.sort((a, b) => {
      const nameA = a.name.toLowerCase();
      const nameB = b.name.toLowerCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    });
  }
}
