import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trim',
})
export class TrimPipe implements PipeTransform {
  // #pipe Not sure I ended up needing this, but the point was to get rid of white-space.
  transform(value: string): string {
    return value.trim();
  }
}
