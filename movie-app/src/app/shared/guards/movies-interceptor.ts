import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvironmentService } from '@shared/services/environment.service';
import { Observable } from 'rxjs';

@Injectable()
export class MoviesInterceptor implements HttpInterceptor {
  constructor(private env: EnvironmentService) {}
  // #interceptor This grabs the request before it goes out and attaches parameters to it
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const environment = this.env.getEnvironment();
    request = request.clone({
      setParams: { api_key: environment.movie_api_key },
    });
    return next.handle(request);
  }
}
