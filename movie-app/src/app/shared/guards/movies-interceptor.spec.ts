import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { EnvironmentService } from '@shared/services/environment.service';

import { SearchService } from '../services/search.service';
import { MoviesInterceptor } from './movies-interceptor';

describe('MoviesInterceptor', () => {
  let environment: EnvironmentService;
  let search: SearchService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [EnvironmentService, { provide: HTTP_INTERCEPTORS, useClass: MoviesInterceptor, multi: true }],
    });

    environment = TestBed.get(EnvironmentService);
    httpMock = TestBed.get(HttpTestingController);
    search = TestBed.get(SearchService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should add an API key query parameter', () => {
    const env = environment.getEnvironment();
    search.search('movie', 'back', 'movie', 1985);

    const url = search.createUrl(env.movie_base_url, { query: 'back', year: 1985, page: null }, ['search', 'movie']);
    const req = httpMock.expectOne(url + '&api_key=' + env.movie_api_key);
    expect(req.request.method).toBe('GET');
    expect(req.request.params.has('api_key')).toEqual(true);
    req.flush([]);
  });
});
