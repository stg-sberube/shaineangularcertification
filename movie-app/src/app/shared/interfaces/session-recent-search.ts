import { Params } from '@angular/router';

import { SearchType } from './search-type';

export interface SessionRecentSearch {
  id: number;
  type: SearchType;
  params: Params;
  date: Date;
}
