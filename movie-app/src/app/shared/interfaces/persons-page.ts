import { PersonsResult } from './persons-result';

export interface PersonsPage {
  page?: number;
  total_results?: number;
  total_pages?: number;
  results?: PersonsResult[];
}
