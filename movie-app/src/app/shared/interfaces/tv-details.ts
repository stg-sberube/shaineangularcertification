import { TvDetailsCreatedBy } from './tv-details-created-by';
import { TvDetailsGenre } from './tv-details-genre';
import { TvDetailsLastEpisodeToAir } from './tv-details-last-episode-to-air';
import { TvDetailsNetwork } from './tv-details-network';
import { TvDetailsProductionCompany } from './tv-details-production-company';
import { TvDetailsSeason } from './tv-details-season';

export interface TvDetails {
  backdrop_path?: string;
  created_by?: TvDetailsCreatedBy[];
  episode_run_time?: number[];
  first_air_date?: string;
  genres?: TvDetailsGenre[];
  homepage?: string;
  id?: number;
  in_production?: boolean;
  languages?: string[];
  last_air_date?: string;
  last_episode_to_air?: TvDetailsLastEpisodeToAir;
  name?: string;
  networks?: TvDetailsNetwork[];
  number_of_episodes?: number;
  number_of_seasons?: number;
  origin_country?: string[];
  original_language?: string;
  original_name?: string;
  overview?: string;
  popularity?: number;
  poster_path?: string;
  production_companies?: TvDetailsProductionCompany[];
  seasons?: TvDetailsSeason[];
  status?: string;
  type?: string;
  vote_average?: number;
  vote_count?: number;
}
