export interface MoviesError {
  status_message?: string;
  status_code?: number;
}
