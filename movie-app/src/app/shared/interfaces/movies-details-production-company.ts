export interface MoviesDetailsProductionCompany {
  id: number;
  logo_path: string;
  name: string;
  origin_country: string;
}
