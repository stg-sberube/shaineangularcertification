export interface HomeResponseMovies {
  dates: HomeResponseDates;
  page: number;
  results: HomeResponseMovies[];
  total_pages: number;
  total_results: number;
}

export interface HomeResponseTV {
  dates: HomeResponseDates;
  page: number;
  results: HomeResponseTV[];
  total_pages: number;
  total_results: number;
}

export interface HomeResponseDates {
  maximum: string;
  minimum: string;
}

export interface HomeResponseMovies {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export interface HomeResponseTV {
  backdrop_path: string;
  first_air_date: string;
  genre_ids: number[];
  id: number;
  name: string;
  origin_country: string[];
  original_language: string;
  original_name: string;
  overview: string;
  popularity: number;
  poster_path: string;
  vote_average: number;
  vote_count: number;
}
