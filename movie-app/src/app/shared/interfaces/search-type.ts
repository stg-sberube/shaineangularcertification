/* #type This is used all over the place in the application, and it simply
restricts the app from using values that will not be allowed in the logic */
export enum SearchType {
  Movie = 'movie',
  Person = 'person',
  TV = 'tv',
}
