import { MoviesDetailsGenre } from './movies-details-genre';
import { MoviesDetailsProductionCompany } from './movies-details-production-company';

export interface MoviesDetails {
  adult: boolean;
  backdrop_path: string;
  budget: number;
  genres: MoviesDetailsGenre[];
  homepage: string;
  id: number;
  imdb_id: string;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  production_companies: MoviesDetailsProductionCompany[];
  release_date: string;
  revenue: number;
  runtime: number;
  status: string;
  tagline: string;
  title: string;
  vote_average: number;
  vote_count: number;
}
