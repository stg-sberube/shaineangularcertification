export interface PersonsResultKnownFor {
  poster_path?: string;
  adult?: boolean;
  overview?: string;
  release_date?: string;
  original_title?: string;
  id?: number;
  media_type?: string;
  original_language?: string;
  title?: string;
  backdrop_path?: string;
  popularity?: number;
  vote_count?: number;
  vote_average?: number;
  first_air_date?: string;
  origin_country?: string[];
  name?: string;
  original_name?: string;
}
