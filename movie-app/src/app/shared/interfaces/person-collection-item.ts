export interface PersonCollectionItem {
  id: number;
  name: string;
  birthday: string;
  department: string;
  profile_picture: string;
}
