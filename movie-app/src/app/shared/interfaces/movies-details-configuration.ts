import { MoviesDetailsConfigurationImages } from './movies-details-configuration-images';

export interface MoviesDetailsConfiguration {
  change_keys: string[];
  images: MoviesDetailsConfigurationImages;
}
