/* #interface Probably just being lazy here, but wanted to simply import them from the same file,
but they are all related at least */
export interface CarouselSlide {
  id: number;
  title: string;
  content_image: string;
  order: number;
  overlay: CarouselOverlay;
}

export interface CarouselOverlay {
  content: string;
  release_date: string;
  actions?: CarouselSlideAction[];
}

export interface CarouselSlideAction {
  label: string;
  callback: () => void;
}
