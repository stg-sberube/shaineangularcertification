import { MoviesResult } from './movies-result';

export interface MoviesPage {
  page?: number;
  total_results?: number;
  total_pages?: number;
  results?: MoviesResult[];
}
