export interface MenuItem {
  label: string;
  content: string;
  callback: () => void;
}
