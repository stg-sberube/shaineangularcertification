export interface User {
  id: number;
  username: string;
  password: string;
  firstname: string;
  lastname: string;
  email: string;
}

export function makeUser() {
  return {
    id: 0,
    username: '',
    password: '',
    firstname: '',
    lastname: '',
    email: '',
  } as User;
}
