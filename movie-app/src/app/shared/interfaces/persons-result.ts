import { PersonsResultKnownFor } from './persons-result-known-for';

export interface PersonsResult {
  id?: number;
  popularity?: number;
  adult?: boolean;
  profile_path?: string;
  name?: string;
  known_for?: PersonsResultKnownFor[];
}
