import { MovieCollectionItem } from './movie-collection-item';
import { PersonCollectionItem } from './person-collection-item';
import { SessionRecentSearch } from './session-recent-search';
import { TvCollectionItem } from './tv-collection-item';

export interface Session {
  userId: number;
  authed?: boolean;
  recentSearches?: SessionRecentSearch[];
  userMovieCollection?: MovieCollectionItem[];
  userPersonCollection?: PersonCollectionItem[];
  userTvCollection?: TvCollectionItem[];
}

export function makeSession() {
  return {
    userId: 0,
    authed: false,
    recentSearches: [],
    userMovieCollection: [],
    userPersonCollection: [],
    userTvCollection: [],
  } as Session;
}
