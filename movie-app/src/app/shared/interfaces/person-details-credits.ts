import { PersonDetailsCreditsDetails } from './person-details-credits-details';

export interface PersonDetailsCredits {
  cast?: PersonDetailsCreditsDetails[];
  crew?: PersonDetailsCreditsDetails[];
  id?: number;
}
