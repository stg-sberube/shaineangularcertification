export interface TvCollectionItem {
  id: number;
  name: string;
  first_air_date: string;
  overview: string;
  background_image: string;
}
