export interface MoviesResult {
  id?: number;
  popularity?: number;
  poster_path?: string;
  overview?: string;
  release_date?: string;
  original_title?: string;
  original_language?: string;
  title?: string;
  backdrop_path?: string;
  first_air_date?: string;
  origin_country?: string[];
  name?: string;
  original_name?: string;
}
