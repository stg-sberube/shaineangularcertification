export interface MovieCollectionItem {
  id: number;
  title: string;
  release_date: string;
  tagline: string;
  background_image: string;
}
