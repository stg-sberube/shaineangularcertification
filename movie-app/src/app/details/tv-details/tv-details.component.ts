import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TvDetails } from '@shared/interfaces/tv-details';
import { AuthService } from '@shared/services/auth.service';
import { DetailsService } from '@shared/services/details.service';
import { Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-tv-details',
  templateUrl: './tv-details.component.html',
  styleUrls: ['./tv-details.component.scss'],
})
export class TvDetailsComponent implements OnDestroy {
  tv$: Observable<TvDetails> = this.detailsService.tvDetails$;
  posterImage$: Observable<string> = this.detailsService.getTvPosterURL();
  backgroundImage$: Observable<string> = this.detailsService.getTvBGImageURL();
  configBaseURL$: Observable<string> = this.detailsService.configuration$.pipe(
    distinctUntilChanged(),
    map(config => config.images.base_url),
    filter(data => data !== null)
  );
  subs: Subscription[] = [];
  historyData = history ? (history.state ? history.state.data : null) : null;
  constructor(
    private route: ActivatedRoute,
    private detailsService: DetailsService,
    private router: Router,
    public auth: AuthService
  ) {
    this.detailsService.getTvDetails(Number(this.route.snapshot.paramMap.get('tvId')));
  }

  back() {
    if (this.historyData) {
      this.router.navigate(['search', 'movie'], { queryParams: this.historyData.params });
    } else {
      this.router.navigate(['search', 'movie']);
    }
  }

  getImageUrl(logoPath: string, baseUrl: string): string {
    if (logoPath && baseUrl) {
      return [baseUrl.replace(/^\/|\/$/g, ''), 'original', logoPath.replace(/^\/|\/$/g, '')].join('/');
    }
    return '';
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => subscription.unsubscribe());
  }
}
