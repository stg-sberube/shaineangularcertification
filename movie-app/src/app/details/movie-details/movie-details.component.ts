import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MoviesDetails } from '@shared/interfaces/movies-details';
import { AuthService } from '@shared/services/auth.service';
import { Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';

import { DetailsService } from '../../shared/services/details.service';

@Component({
  selector: 'app-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
})
export class MovieDetailsComponent implements OnDestroy {
  movie$: Observable<MoviesDetails> = this.detailsService.movieDetails$;
  posterImage$: Observable<string> = this.detailsService.getMoviePosterURL();
  backgroundImage$: Observable<string> = this.detailsService.getMovieBGImageURL();
  configBaseURL$: Observable<string> = this.detailsService.configuration$.pipe(
    distinctUntilChanged(),
    map(config => config.images.base_url),
    filter(data => data !== null)
  );
  subs: Subscription[] = [];
  historyData = history ? (history.state ? history.state.data : null) : null;
  constructor(
    private route: ActivatedRoute,
    private detailsService: DetailsService,
    private router: Router,
    public auth: AuthService
  ) {
    this.detailsService.getMovieDetails(Number(this.route.snapshot.paramMap.get('movieId')));
  }

  back() {
    if (this.historyData) {
      this.router.navigate(['search', 'movie'], { queryParams: this.historyData.params });
    } else {
      this.router.navigate(['search', 'movie']);
    }
  }

  getImageUrl(logoPath: string, baseUrl: string): string {
    if (logoPath && baseUrl) {
      return [baseUrl.replace(/^\/|\/$/g, ''), 'original', logoPath.replace(/^\/|\/$/g, '')].join('/');
    }
    return '';
  }

  /* #lifecycle-hooks This ensures that any subscriptions this component creates is destroyed as we exit this component */
  ngOnDestroy() {
    this.subs.forEach(subscription => subscription.unsubscribe());
  }
}
