import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonDetails } from '@shared/interfaces/person-details';
import { PersonDetailsCredits } from '@shared/interfaces/person-details-credits';
import { AuthService } from '@shared/services/auth.service';
import { Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';

import { DetailsService } from '../../shared/services/details.service';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.scss'],
})
export class PersonDetailsComponent implements OnDestroy {
  person$: Observable<PersonDetails> = this.detailsService.personDetails$;
  profileImage$: Observable<string> = this.detailsService.getProfileURL();
  personCredits$: Observable<PersonDetailsCredits> = this.detailsService.getPersonCredits();
  configBaseURL$: Observable<string> = this.detailsService.configuration$.pipe(
    distinctUntilChanged(),
    map(config => config.images.base_url),
    filter(data => data !== null)
  );
  subs: Subscription[] = [];
  historyData = history ? (history.state ? history.state.data : null) : null;
  constructor(
    private route: ActivatedRoute,
    private detailsService: DetailsService,
    private router: Router,
    public auth: AuthService
  ) {
    this.detailsService.getPersonDetails(Number(this.route.snapshot.paramMap.get('personId')));
  }

  back() {
    if (this.historyData) {
      this.router.navigate(['search', 'person'], { queryParams: this.historyData.params });
    } else {
      this.router.navigate(['search', 'person']);
    }
  }

  getImageUrl(creditPath: string, baseUrl: string): string {
    if (creditPath && baseUrl) {
      return [baseUrl.replace(/^\/|\/$/g, ''), 'original', creditPath.replace(/^\/|\/$/g, '')].join('/');
    }
    return '';
  }

  shortenString(str: string): string {
    if (str.length > 55) {
      const newStr = str.substr(0, 55);
      if (newStr.substr(-1) === ' ') {
        return newStr.trim() + '...';
      } else {
        return newStr + '...';
      }
    } else {
      if (str.length > 1) {
        return str;
      } else {
        return 'No biography provided...';
      }
    }
  }

  ngOnDestroy() {
    this.subs.forEach(subscription => subscription.unsubscribe());
  }
}
