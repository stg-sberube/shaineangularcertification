import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatCardModule, MatTooltipModule } from '@angular/material';
import { SharedModule } from '@shared/shared.module';

import { DetailsRoutingModule } from './details-routing.module';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { TvDetailsComponent } from './tv-details/tv-details.component';

@NgModule({
  declarations: [MovieDetailsComponent, PersonDetailsComponent, TvDetailsComponent],
  imports: [CommonModule, MatCardModule, MatButtonModule, MatTooltipModule, SharedModule, DetailsRoutingModule],
})
export class DetailsModule {}
