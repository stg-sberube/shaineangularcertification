import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { TvDetailsComponent } from './tv-details/tv-details.component';

const routes: Routes = [
  {
    path: 'movie/:movieId',
    component: MovieDetailsComponent,
  },
  {
    path: 'person/:personId',
    component: PersonDetailsComponent,
  },
  {
    path: 'tv/:tvId',
    component: TvDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsRoutingModule {}
