# STG Angular Certification
Version 1.0

#### Key points to understand:
* Typescript
* OOP basics
    * Classes
    * Modules
    * Interfaces
* Higher-order functions
* “This” keyword
* Arrow functions
* Arrays
* Map, Filter, Reduce Functions
* Destructuring
* Promises
* Block-scoped variables Let and Const

## Introduction
* What is Angular?
* How is it different from AngularJS?
* What is the Angular CLI?

## Modules
* https://angular.io/guide/module-types
* What benefits does breaking your code into modules provide?
* Frequently used modules (BrowserModule, CommonModule, FormsModule, ReactiveFormsModule, RouterModule, HttpClientModule)
* Importing modules

## Components
* Inputs
* Outputs
* Lifecycle hooks
* ViewEncapsulation
* ChangeDetection
* ViewChild/ViewChildren
* ***Task - demonstrate component reusability using inputs and outputs***

# Forms
* Template driven forms
* Reactive forms
* Validation (built-in validators, custom validators)
* ***Task - create a parent/child component that can share form data using reactive forms***

## Directives
* *ngIf
* *ngFor
* *ngTemplate
* *ngSwitch
* exportAs
* HostBinding
* HostListener

# Pipes
* Built-in Pipes (Date, Uppercase, Lowercase, Currency and Percent)
* Custom Pipes (@Pipe)
* AsyncPipe
* ***Task - create a pipe that capitalizes every other letter in each word in an array***
* ***A pipe that they may have to use pure=false to make it work***
* ***A phone number pipe***

# Services
* Dependency Injection (what does this mean)
* Register Service

# Observables
* Subjects
* Hot vs cold
* Create an Observable
* Operators (map, filter, concat, flatmap)
* Error Handling

# Routing & Navigation
* RouterModule
* Basic config and route registration
* ActivatedRoute Service
* Router Events
* Guards
* Lazy loading

# TypeScript
* Basic config
* Type definitions
* Interfaces

## CLI Commands
* ng add
* ng generate
* ng build
* ng serve
* Others?


## Free Resources
###  [Angular Basics]
* [Official Documentation](https://angular.io/)
* [Official Tutorial](https://angular.io/tutorial)
* [Angular Styleguide](https://angular.io/guide/styleguide)
* [Netanel Basal - Blog](https://netbasal.com/) (covers a lot of Angular concepts)
* [Angular In Depth](https://blog.angularindepth.com/) (very detailed articles on how Angular internals work)
* [Thoughtram - Blog](https://blog.thoughtram.io/categories/angular-2/)
* [Todd Motto - Blog](https://toddmotto.com/)
* [Using ExportAs - Directives](https://netbasal.com/angular-2-take-advantage-of-the-exportas-property-81374ce24d26)


### Typescript
* [Official Documentation](https://www.typescriptlang.org/docs/home.html)
* [Official Handbook](https://www.typescriptlang.org/docs/handbook/basic-types.html)
* [TS deepdive Gitbook by Basarat Syed](https://basarat.gitbooks.io/typescript/)
* [Object Oriented Programming with TypeScript](http://rachelappel.com/write-object-oriented-javascript-with-typescript/)
* [Functional programming with TypeScript](https://vsavkin.com/functional-typescript-316f0e003dc6)
* [Advanced TypeScript Types with Examples](https://levelup.gitconnected.com/advanced-typescript-types-with-examples-1d144e4eda9e)

### Rxjs
* [What is RxJS](https://css-tricks.com/animated-intro-rxjs)
* [The introduction to reactive programming with RxJS](https://gist.github.com/staltz/868e7e9bc2a7b8c1f754)
* [RxJS Video Course](https://www.youtube.com/watch?v=Tux1nhBPl_w) <span style="font-size:smaller">(video)</span>
* [Learn RxJS - Organised API Reference](https://www.learnrxjs.io/)
* [RxMarbles: Interactive diagrams of Rx Observables](http://rxmarbles.com/)
* [RxJS Docs](https://rxjs-dev.firebaseapp.com/)
* [RxJS Combinations Animated Examples](https://blog.angularindepth.com/learn-to-combine-rxjs-sequences-with-super-intuitive-interactive-diagrams-20fce8e6511)
* [Async Pipe](https://blog.angularindepth.com/angular-question-rxjs-subscribe-vs-async-pipe-in-component-templates-c956c8c0c794)
* [When to Unsubscribe](https://netbasal.com/when-to-unsubscribe-in-angular-d61c6b21bad3)
* [On The Subject Of Subjects](https://medium.com/@benlesh/on-the-subject-of-subjects-in-rxjs-2b08b7198b93)
* [RxJS Error Handling](https://blog.angular-university.io/rxjs-error-handling/)

### State Management
* [NGRX - Angular state management library](https://github.com/ngrx/platform)
* [Quick intro to Redux style state management with NGRX](https://www.youtube.com/watch?v=f97ICOaekNU) <span style="font-size:smaller">(video)</span>
* [State management with NGRX tutorial](https://coursetro.com/posts/code/151/Angular-Ngrx-Store-Tutorial---Learn-Angular-State-Management)
* [In depth introduction to NGRX/Redux state management](https://medium.com/@bencabanes/redux-introduction-with-ngrx-part-1-store-application-state-2c47c35376ea)
* [NGRX advanced patterns & techniques](https://blog.nrwl.io/ngrx-patterns-and-techniques-f46126e2b1e5)

### Alternatives to NGRX
* [NGXS](https://github.com/ngxs/store)
* [Akita](https://netbasal.com/introducing-akita-a-new-state-management-pattern-for-angular-applications-f2f0fab5a8)

## Paid Resources
(some paid resources may be reimbursable)
* [STG Training Policy](https://docs.google.com/document/d/1rKuMBH8plra7Uv_MSsL_5BQrn4-EBlM78vpnk0ATp2I/edit)

### Javascript
* [Learn ES6 (Egghead - Jacob Carter)](https://egghead.io/courses/learn-es6-ecmascript-2015)
* [Accelerated Javascript Training (Udemy - Maximilian Schwarzmuller)](https://www.udemy.com/javascript-bootcamp-2016/)
* [ES6 Javascript: The Complete Developer's Guide (Udemy - Stephen Grider)](https://www.udemy.com/javascript-es6-tutorial/)
* [ES6 For Everyone (Wes Bos)](https://es6.io/)

Angular
* [Angular Udemy video Course from, beginner to Pro level](https://www.udemy.com/the-complete-guide-to-angular-2/)
